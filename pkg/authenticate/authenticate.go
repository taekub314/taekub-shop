package authenticate

import (
	"errors"
	"fmt"
	"math"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/users"
)

type TokenType string

const (
	Access  TokenType = "access"
	Refresh TokenType = "refresh"
	Admin   TokenType = "admin"
	ApiKey  TokenType = "apiKey"
)

type IAuthenticate interface {
	SignToken() string
}

// type IAuthenticateAdmin interface {
// 	SignAdminToken() string
// }

// type IAuthenticateApiKey interface {
// 	SignApiKey() string
// }

type authenticate struct {
	payload *authenticatePayload
	conf    config.IJwtConfig
}

type authenticateAdmin struct {
	*authenticate
}

type authenticateApikey struct {
	*authenticate
}

type authenticatePayload struct {
	Claims *users.UserClaims `json:"claims"`
	jwt.RegisteredClaims
}

func (auth *authenticate) SignToken() string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, auth.payload)
	sign, _ := token.SignedString(auth.conf.SecretKey())

	return sign
}

func (auth *authenticateAdmin) SignAdminToken() string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, auth.payload)
	sign, _ := token.SignedString(auth.conf.AdminKey())

	return sign
}

func (auth *authenticateApikey) SignApiKey() string {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, auth.payload)
	sign, _ := token.SignedString(auth.conf.ApiKey())

	return sign
}

func ParseToken(conf config.IJwtConfig, tokenString string) (*authenticatePayload, error) {
	token, err := jwt.ParseWithClaims(tokenString, &authenticatePayload{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("signing method is invalid")
		}

		return conf.SecretKey(), nil
	})

	if err != nil {
		if errors.Is(err, jwt.ErrTokenMalformed) {
			return nil, fmt.Errorf("token format invalid")
		} else if errors.Is(err, jwt.ErrTokenExpired) {
			return nil, fmt.Errorf("token has expired")
		}
	}

	if claims, ok := token.Claims.(*authenticatePayload); ok {
		return claims, nil
	} else {
		return nil, fmt.Errorf("claims type is invalid")
	}
}

func ParseAdminToken(conf config.IJwtConfig, tokenString string) (*authenticatePayload, error) {
	token, err := jwt.ParseWithClaims(tokenString, &authenticatePayload{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("signing method is invalid")
		}

		return conf.AdminKey(), nil
	})

	if err != nil {
		if errors.Is(err, jwt.ErrTokenMalformed) {
			return nil, fmt.Errorf("token format invalid")
		} else if errors.Is(err, jwt.ErrTokenExpired) {
			return nil, fmt.Errorf("token has expired")
		}
	}

	if claims, ok := token.Claims.(*authenticatePayload); ok {
		return claims, nil
	} else {
		return nil, fmt.Errorf("claims type is invalid")
	}
}

func ParseApiKey(conf config.IJwtConfig, tokenString string) (*authenticatePayload, error) {
	token, err := jwt.ParseWithClaims(tokenString, &authenticatePayload{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("signing method is invalid")
		}

		return conf.ApiKey(), nil
	})

	if err != nil {
		if errors.Is(err, jwt.ErrTokenMalformed) {
			return nil, fmt.Errorf("token format invalid")
		} else if errors.Is(err, jwt.ErrTokenExpired) {
			return nil, fmt.Errorf("token has expired")
		}
	}

	if claims, ok := token.Claims.(*authenticatePayload); ok {
		return claims, nil
	} else {
		return nil, fmt.Errorf("claims type is invalid")
	}
}

func NewAuthenticate(tokenType TokenType, conf config.IJwtConfig, claims *users.UserClaims) (IAuthenticate, error) {
	switch tokenType {
	case Access:
		return newAccessToken(conf, claims), nil
	case Refresh:
		return newRefreshToken(conf, claims), nil
	case Admin:
		return newAdminToken(conf), nil
	case ApiKey:
		return newApiKey(conf), nil
	default:
		return nil, fmt.Errorf("unknow token type")
	}
}

func newAccessToken(conf config.IJwtConfig, claims *users.UserClaims) IAuthenticate {
	return &authenticate{
		conf: conf,
		payload: &authenticatePayload{
			Claims: claims,
			RegisteredClaims: jwt.RegisteredClaims{
				Issuer:    "taekub-shop-api",
				Subject:   "access-token",
				Audience:  []string{"customer", "admin"},
				ExpiresAt: jwtTimeDurationCal(conf.AccessExpiresAt()),
				NotBefore: jwt.NewNumericDate(time.Now()),
				IssuedAt:  jwt.NewNumericDate(time.Now()),
			},
		},
	}
}

func newRefreshToken(conf config.IJwtConfig, claims *users.UserClaims) IAuthenticate {
	return &authenticate{
		conf: conf,
		payload: &authenticatePayload{
			Claims: claims,
			RegisteredClaims: jwt.RegisteredClaims{
				Issuer:    "taekub-shop-api",
				Subject:   "refresh-token",
				Audience:  []string{"customer", "admin"},
				ExpiresAt: jwtTimeDurationCal(conf.RefreshExpiresAt()),
				NotBefore: jwt.NewNumericDate(time.Now()),
				IssuedAt:  jwt.NewNumericDate(time.Now()),
			},
		},
	}
}

func newAdminToken(conf config.IJwtConfig) IAuthenticate {
	return &authenticateAdmin{
		authenticate: &authenticate{
			conf: conf,
			payload: &authenticatePayload{
				Claims: nil,
				RegisteredClaims: jwt.RegisteredClaims{
					Issuer:    "taekub-shop-api",
					Subject:   "admin-token",
					Audience:  []string{"admin"},
					ExpiresAt: jwtTimeDurationCal(300),
					NotBefore: jwt.NewNumericDate(time.Now()),
					IssuedAt:  jwt.NewNumericDate(time.Now()),
				},
			},
		},
	}
}

func newApiKey(conf config.IJwtConfig) IAuthenticate {
	return &authenticateApikey{
		authenticate: &authenticate{
			conf: conf,
			payload: &authenticatePayload{
				Claims: nil,
				RegisteredClaims: jwt.RegisteredClaims{
					Issuer:    "taekub-shop-api",
					Subject:   "api-key",
					Audience:  []string{"admin", "customer"},
					ExpiresAt: jwt.NewNumericDate(time.Now().AddDate(2, 0, 0)),
					NotBefore: jwt.NewNumericDate(time.Now()),
					IssuedAt:  jwt.NewNumericDate(time.Now()),
				},
			},
		},
	}
}

func RepeatRefreshToken(conf config.IJwtConfig, claim *users.UserClaims, exp int64) string {
	obj := &authenticate{
		conf: conf,
		payload: &authenticatePayload{
			Claims: claim,
			RegisteredClaims: jwt.RegisteredClaims{
				Issuer:    "taekub-shop-api",
				Subject:   "refresh-token",
				Audience:  []string{"customer", "admin"},
				ExpiresAt: jwtTimeRepeatAdapter(exp),
				NotBefore: jwt.NewNumericDate(time.Now()),
				IssuedAt:  jwt.NewNumericDate(time.Now()),
			},
		},
	}

	return obj.SignToken()
}

func jwtTimeDurationCal(t int) *jwt.NumericDate {
	return jwt.NewNumericDate(time.Now().Add(time.Duration(int64(t) * int64(math.Pow10(9)))))
}

func jwtTimeRepeatAdapter(t int64) *jwt.NumericDate {
	return jwt.NewNumericDate(time.Unix(t, 0))
}
