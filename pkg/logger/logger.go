package logger

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/taekub314/taekub-shop/pkg/utilities"
)

type ILogger interface {
	Print() ILogger
	Save()
	SetQuery(ctx *fiber.Ctx)
	SetBody(ctx *fiber.Ctx)
	SetResponse(res any)
}

type logger struct {
	Time       string `json:"time"`
	Ip         string `json:"ip"`
	Method     string `json:"method"`
	StatusCode int    `json:"status_code"`
	Path       string `json:"path"`
	Query      any    `json:"query"`
	Body       any    `json:"body"`
	Response   any    `json:"response"`
}

func InitLogger(ctx *fiber.Ctx, res any) ILogger {
	l := &logger{
		Time:       time.Now().Local().Format("2006-01-02 15:04:05"),
		Ip:         ctx.IP(),
		Method:     ctx.Method(),
		StatusCode: ctx.Response().StatusCode(),
		Path:       ctx.Path(),
	}

	l.SetQuery(ctx)
	l.SetBody(ctx)
	l.SetResponse(res)

	return l
}

func (l *logger) Print() ILogger {
	utilities.Debug(l)
	return l
}

func (l *logger) Save() {
	data := utilities.Output(l)
	filename := fmt.Sprintf("./assets/logs/logger_%v.txt", strings.ReplaceAll(time.Now().Format("2006-01-02"), "-", ""))
	file, err := os.OpenFile(filename, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)

	if err != nil {
		log.Fatalf("Error opening file: %v", err)
	}

	defer file.Close()
	file.WriteString(string(data) + "")
}

func (l *logger) SetQuery(ctx *fiber.Ctx) {
	var body any

	if err := ctx.QueryParser(&body); err != nil {
		log.Printf("Query parser error: %v", err)
	}

	l.Query = body
}

func (l *logger) SetBody(ctx *fiber.Ctx) {
	var body any

	if err := ctx.BodyParser(&body); err != nil {
		log.Printf("Body parser error: %v", err)
	}

	switch l.Path {
	case "v1/users/signup":
		l.Body = "Never gonna giveup you go"
	default:
		l.Body = body
	}
}

func (l *logger) SetResponse(res any) {
	l.Response = res
}
