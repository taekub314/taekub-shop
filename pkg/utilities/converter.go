package utilities

func BinaryConverter(num int, bits int) []int {
	factor := num
	result := make([]int, bits)

	for factor >= 0 && num > 0 {
		factor = num % 2
		num /= 2
		result[bits-1] = factor
		bits--
	}

	return result
}
