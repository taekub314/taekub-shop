package databases

import (
	"log"

	_ "github.com/jackc/pgx/v5/stdlib"
	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/config"
)

func DbConnect(conf config.IDbConfig) *sqlx.DB {
	// Connect DB
	db, err := sqlx.Connect("pgx", conf.Url())

	if err != nil {
		log.Fatalf("Connect to DB fail %v", err)
	}

	db.DB.SetMaxOpenConns(conf.MaxOpenCons())
	return db
}
