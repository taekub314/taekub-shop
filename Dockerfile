FROM golang:1.21-alpine3.18 AS build

WORKDIR /app

COPY . ./

RUN go mod download

RUN CGO_ENABLED=0 go build -o /app/api

# Deploy
FROM gcr.io/distroless/static-debian11

COPY --from=build /app/api /bin
COPY .env.prod /bin
COPY /assets /bin/assets

EXPOSE 3000

ENTRYPOINT [ "/bin/api", "/bin/.env.prod" ]