package config

import (
	"fmt"
	"log"
	"math"
	"strconv"
	"time"

	"github.com/joho/godotenv"
)

type IConfig interface {
	App() IAppConfig
	Db() IDbConfig
	Jwt() IJwtConfig
}

type IAppConfig interface {
	Url() string
	Name() string
	Version() string
	BodyLimit() int
	ReadTimeout() time.Duration
	WriteTimeout() time.Duration
	FileLimit() int
	GCPBucket() string
	ProductBucket() string
}

type IDbConfig interface {
	Url() string
	MaxOpenCons() int
}

type IJwtConfig interface {
	ApiKey() []byte
	AdminKey() []byte
	SecretKey() []byte
	AccessExpiresAt() int
	RefreshExpiresAt() int
	SetAccessExpires(t int)
	SetRefreshExpires(t int)
}

type config struct {
	app *app
	db  *db
	jwt *jwt
}

type app struct {
	host          string
	port          int
	name          string
	version       string
	bodyLimit     int
	readTimeout   time.Duration
	writeTimeout  time.Duration
	fileLimit     int
	gcpBucket     string
	productBucket string
}

func (conf *config) App() IAppConfig {
	return conf.app
}

func (a *app) Url() string                 { return fmt.Sprintf("%s:%d", a.host, a.port) }
func (a *app) Name() string                { return a.name }
func (a *app) Version() string             { return a.version }
func (a *app) BodyLimit() int              { return a.bodyLimit }
func (a *app) ReadTimeout() time.Duration  { return a.readTimeout }
func (a *app) WriteTimeout() time.Duration { return a.writeTimeout }
func (a *app) FileLimit() int              { return a.fileLimit }
func (a *app) GCPBucket() string           { return a.gcpBucket }
func (a *app) ProductBucket() string       { return a.productBucket }

type db struct {
	host          string
	port          int
	protocal      string
	username      string
	password      string
	database      string
	sslMode       string
	maxConnection int
}

func (conf *config) Db() IDbConfig {
	return conf.db
}

func (d *db) Url() string {
	return fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=%s",
		d.host,
		d.port,
		d.username,
		d.password,
		d.database,
		d.sslMode,
	)
}

func (d *db) MaxOpenCons() int { return d.maxConnection }

type jwt struct {
	apiKey           string
	adminKey         string
	secretKey        string
	accessExpiresAt  int
	refreshExpiresAt int
}

func (conf *config) Jwt() IJwtConfig {
	return conf.jwt
}

func (j *jwt) ApiKey() []byte          { return []byte(j.apiKey) }
func (j *jwt) AdminKey() []byte        { return []byte(j.adminKey) }
func (j *jwt) SecretKey() []byte       { return []byte(j.secretKey) }
func (j *jwt) AccessExpiresAt() int    { return j.accessExpiresAt }
func (j *jwt) RefreshExpiresAt() int   { return j.refreshExpiresAt }
func (j *jwt) SetAccessExpires(t int)  { j.accessExpiresAt = t }
func (j *jwt) SetRefreshExpires(t int) { j.refreshExpiresAt = t }

func LoadConfig(path string) IConfig {
	envMap, err := godotenv.Read(path)

	if err != nil {
		log.Fatalf("Load ENV failed: %v", err)
	}

	return &config{
		app: &app{
			host: envMap["APP_HOST"],
			port: func() int {
				port, err := strconv.Atoi(envMap["APP_PORT"])

				if err != nil {
					log.Fatalf("Load Port failed: %v", err)
				}

				return port
			}(),
			name:    envMap["APP_NAME"],
			version: envMap["APP_VERSION"],
			bodyLimit: func() int {
				bodyLimit, err := strconv.Atoi(envMap["APP_BODY_LIMIT"])

				if err != nil {
					log.Fatalf("Load Body limit failed: %v", err)
				}

				return bodyLimit
			}(),
			readTimeout: func() time.Duration {
				readTimeout, err := strconv.Atoi(envMap["APP_READ_TIMEOUT"])

				if err != nil {
					log.Fatalf("Load Read timeout failed: %v", err)
				}

				return time.Duration(int64(readTimeout) * int64(math.Pow10(9)))
			}(),
			writeTimeout: func() time.Duration {
				writeTimeout, err := strconv.Atoi(envMap["APP_WRITE_TIMEOUT"])

				if err != nil {
					log.Fatalf("Load Write timeout failed: %v", err)
				}

				return time.Duration(int64(writeTimeout) * int64(math.Pow10(9)))
			}(),
			fileLimit: func() int {
				fileLimit, err := strconv.Atoi(envMap["APP_FILE_LIMIT"])

				if err != nil {
					log.Fatalf("Load File limit failed: %v", err)
				}

				return fileLimit
			}(),
			gcpBucket:     envMap["APP_GCP_BUCKET"],
			productBucket: envMap["APP_BUCKET_PRODUCT_PATH"],
		},
		db: &db{
			host: envMap["DB_HOST"],
			port: func() int {
				port, err := strconv.Atoi(envMap["DB_PORT"])

				if err != nil {
					log.Fatalf("Load DB Port failed: %v", err)
				}

				return port
			}(),
			protocal: envMap["DB_PROTOCAL"],
			username: envMap["DB_USERNAME"],
			password: envMap["DB_PASSWORD"],
			database: envMap["DB_DATABASE"],
			sslMode:  envMap["DB_SSL_MODE"],
			maxConnection: func() int {
				maxConnection, err := strconv.Atoi(envMap["DB_MAX_CONNECTIONS"])

				if err != nil {
					log.Fatalf("Load DB Max Connections failed: %v", err)
				}

				return maxConnection
			}(),
		},
		jwt: &jwt{
			apiKey:    envMap["JWT_API_KEY"],
			adminKey:  envMap["JWT_ADMIN_KEY"],
			secretKey: envMap["JWT_SECRET_KEY"],
			accessExpiresAt: func() int {
				accessExpiresAt, err := strconv.Atoi(envMap["JWT_ACCESS_EXPIRES"])

				if err != nil {
					log.Fatalf("Load Access Expires failed: %v", err)
				}

				return accessExpiresAt
			}(),
			refreshExpiresAt: func() int {
				refreshExpiresAt, err := strconv.Atoi(envMap["JWT_REFRESH_EXPIRES"])

				if err != nil {
					log.Fatalf("Load Refresh Expires failed: %v", err)
				}

				return refreshExpiresAt
			}(),
		},
	}
}
