package main

import (
	"os"

	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/servers"
	"gitlab.com/taekub314/taekub-shop/pkg/databases"
)

func envPath() string {
	if len(os.Args) == 1 {
		return ".env"
	}

	return os.Args[1]
}

func main() {
	conf := config.LoadConfig(envPath())
	db := databases.DbConnect(conf.Db())
	defer db.Close()

	servers.NewServer(db, conf).Start()
}
