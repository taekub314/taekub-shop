package test

import (
	"encoding/json"

	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/servers"
	"gitlab.com/taekub314/taekub-shop/pkg/databases"
)

func Setup() servers.IModuleFactory {
	conf := config.LoadConfig("../.env.test")
	db := databases.DbConnect(conf.Db())
	// defer db.Close()

	serv := servers.NewServer(db, conf)
	return servers.InitModule(nil, serv.GetServer(), nil)
}

func CompressToJson(obj any) string {
	result, _ := json.Marshal(&obj)
	return string(result)
}
