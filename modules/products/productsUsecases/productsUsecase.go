package productsUsecases

import (
	"math"

	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/products"
	"gitlab.com/taekub314/taekub-shop/modules/products/productsRepositories"
)

type IProductsUsecase interface {
	FindOneProduct(productId string) (*products.Product, error)
	FindProducts(req *products.ProductFilter) *entities.PaginationRes
	InsertProduct(req *products.Product) (*products.Product, error)
	UpdateProduct(req *products.Product) (*products.Product, error)
	DeleteProduct(productId string) error
}

type productsUsecase struct {
	productsRepository productsRepositories.IProductsRepository
}

func ProductsUsecase(productsRepository productsRepositories.IProductsRepository) IProductsUsecase {
	return &productsUsecase{
		productsRepository: productsRepository,
	}
}

func (u *productsUsecase) FindOneProduct(productId string) (*products.Product, error) {
	product, err := u.productsRepository.FindOneProduct(productId)

	if err != nil {
		return nil, err
	}

	return product, nil
}

func (u *productsUsecase) FindProducts(req *products.ProductFilter) *entities.PaginationRes {
	result, count := u.productsRepository.FindProducts(req)

	return &entities.PaginationRes{
		Data:      result,
		Page:      req.Page,
		PageSize:  req.PageSize,
		TotalItem: count,
		TotalPage: int(math.Ceil(float64(count) / float64(req.PageSize))),
	}
}

func (u *productsUsecase) InsertProduct(req *products.Product) (*products.Product, error) {
	result, err := u.productsRepository.InsertProduct(req)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (u *productsUsecase) UpdateProduct(req *products.Product) (*products.Product, error) {
	result, err := u.productsRepository.UpdateProduct(req)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (u *productsUsecase) DeleteProduct(productId string) error {
	if err := u.productsRepository.DeleteProduct(productId); err != nil {
		return err
	}

	return nil
}
