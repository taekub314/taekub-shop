package productsHandler

import (
	"fmt"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/appinfo"
	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/files"
	"gitlab.com/taekub314/taekub-shop/modules/files/filesUsecases"
	"gitlab.com/taekub314/taekub-shop/modules/products"
	"gitlab.com/taekub314/taekub-shop/modules/products/productsUsecases"
)

type productHandlerErrCode string

const (
	findOneProductErr productHandlerErrCode = "product-001"
	findProductsErr   productHandlerErrCode = "product-002"
	addProductErr     productHandlerErrCode = "product-003"
	updateProductErr  productHandlerErrCode = "product-004"
	removeProductErr  productHandlerErrCode = "product-005"
)

type IProductsHandler interface {
	FindOneProduct(ctx *fiber.Ctx) error
	FindProducts(ctx *fiber.Ctx) error
	AddProduct(ctx *fiber.Ctx) error
	UpdateProduct(ctx *fiber.Ctx) error
	RemoveProduct(ctx *fiber.Ctx) error
}

type productsHandler struct {
	conf            config.IConfig
	productsUsecase productsUsecases.IProductsUsecase
	filesUsecases   filesUsecases.IFilesUsecase
}

func ProductsHandler(conf config.IConfig, productsUsecase productsUsecases.IProductsUsecase, filesUsecases filesUsecases.IFilesUsecase) IProductsHandler {
	return &productsHandler{
		conf:            conf,
		productsUsecase: productsUsecase,
		filesUsecases:   filesUsecases,
	}
}

func (h *productsHandler) FindOneProduct(ctx *fiber.Ctx) error {
	productId := strings.Trim(ctx.Params("product_id", ""), " ")

	if productId == "" {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(findOneProductErr),
			"invalid product id",
		).Resp()
	}

	product, err := h.productsUsecase.FindOneProduct(productId)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(findOneProductErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, product).Resp()
}

func (h *productsHandler) FindProducts(ctx *fiber.Ctx) error {
	req := &products.ProductFilter{
		PaginationReq: &entities.PaginationReq{},
		SortReq:       &entities.SortReq{},
	}

	if err := ctx.QueryParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(findProductsErr),
			"invalid criteria search",
		).Resp()
	}

	h.initialSort(req)
	h.initialPagination(req)

	if req.Page < 1 {
		req.Page = 1
	}

	if req.PageSize < 5 {
		req.PageSize = 5
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, h.productsUsecase.FindProducts(req)).Resp()
}

func (h *productsHandler) initialSort(req *products.ProductFilter) {
	orderBy := map[string]string{
		"id":    `"p"."id"`,
		"title": `"p"."title"`,
		"price": `"p"."price"`,
	}

	if orderBy[req.OrderBy] == "" {
		req.OrderBy = orderBy["title"]
	} else {
		req.OrderBy = orderBy[req.OrderBy]
	}

	sort := map[string]string{
		"ASC":  "ASC",
		"DESC": "DESC",
	}

	if sort[strings.ToUpper(req.Sort)] == "" {
		req.Sort = sort["DESC"]
	} else {
		req.Sort = sort[strings.ToUpper(req.Sort)]
	}
}

func (h *productsHandler) initialPagination(req *products.ProductFilter) {
	if req.Page < 1 {
		req.Page = 1
	}

	if req.PageSize < 5 {
		req.PageSize = 5
	}
}

func (h *productsHandler) AddProduct(ctx *fiber.Ctx) error {
	req := &products.Product{
		Category: &appinfo.Category{},
		Images:   make([]*entities.Image, 0),
	}

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(addProductErr),
			err.Error(),
		).Resp()
	}

	if req.Category.Id <= 0 {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(addProductErr),
			"invalid category id",
		).Resp()
	}

	product, err := h.productsUsecase.InsertProduct(req)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(addProductErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusCreated, product).Resp()
}

func (h *productsHandler) UpdateProduct(ctx *fiber.Ctx) error {
	productId := strings.Trim(ctx.Params("product_id"), " ")

	req := &products.Product{
		Category: &appinfo.Category{},
		Images:   make([]*entities.Image, 0),
	}

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(updateProductErr),
			err.Error(),
		).Resp()
	}

	req.Id = productId
	product, err := h.productsUsecase.UpdateProduct(req)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(updateProductErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, product).Resp()
}

func (h *productsHandler) RemoveProduct(ctx *fiber.Ctx) error {
	productId := strings.Trim(ctx.Params("product_id"), " ")

	product, err := h.productsUsecase.FindOneProduct(productId)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(removeProductErr),
			err.Error(),
		).Resp()
	}

	deleteFilesReq := make([]*files.DeleteFileReq, 0)

	for _, p := range product.Images {
		deleteFilesReq = append(deleteFilesReq, &files.DeleteFileReq{
			Destination: fmt.Sprintf("%s/%s", h.conf.App().ProductBucket(), p.FileName),
		})
	}

	if err := h.filesUsecases.DeleteFilesOnGCP(deleteFilesReq); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(removeProductErr),
			err.Error(),
		).Resp()
	}

	if err := h.productsUsecase.DeleteProduct(productId); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(removeProductErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusNoContent, nil).Resp()
}
