package productsPatterns

type updateProductEngineer struct {
	builder IUpdateProductBuilder
}

func UpdateProductEngineer(builder IUpdateProductBuilder) *updateProductEngineer {
	return &updateProductEngineer{
		builder: builder,
	}
}

func (e *updateProductEngineer) UpdateProduct() error {
	e.builder.initTransaction()
	e.builder.initQuery()
	e.summaryQueryFields()
	e.builder.closeQuery()

	// update product
	if err := e.builder.updateProduct(); err != nil {
		return err
	}

	// update category
	if err := e.builder.updateCategory(); err != nil {
		return err
	}

	if e.builder.getImagesLen() > 0 {
		// delete old image
		if err := e.builder.deleteOldImages(); err != nil {
			return err
		}

		// insert image
		if err := e.builder.insertImages(); err != nil {
			return err
		}
	}

	// commit transaction
	if err := e.builder.commit(); err != nil {
		return err
	}

	return nil
}

func (e *updateProductEngineer) summaryQueryFields() {
	e.builder.updateTitleQuery()
	e.builder.updateDescriptionQuery()
	e.builder.updateCategory()
	e.builder.updatePriceQuery()

	fields := e.builder.getQueryFields()

	for i := range fields {
		query := e.builder.getQuery()

		if i != len(fields)-1 {
			e.builder.setQuery(query + fields[i] + ",")
		} else {
			e.builder.setQuery(query + fields[i])
		}
	}
}
