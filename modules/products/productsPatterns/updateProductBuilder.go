package productsPatterns

import (
	"context"
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/files"
	"gitlab.com/taekub314/taekub-shop/modules/files/filesUsecases"
	"gitlab.com/taekub314/taekub-shop/modules/products"
)

type IUpdateProductBuilder interface {
	initTransaction() error
	initQuery()
	updateTitleQuery()
	updateDescriptionQuery()
	updatePriceQuery()
	updateCategory() error
	insertImages() error
	getOldImages() []*entities.Image
	deleteOldImages() error
	closeQuery()
	updateProduct() error
	getQueryFields() []string
	getValues() []any
	getQuery() string
	setQuery(query string)
	getImagesLen() int
	commit() error
}

type updateProductBuilder struct {
	app            config.IAppConfig
	db             *sqlx.DB
	txn            *sqlx.Tx
	req            *products.Product
	fileUsecase    filesUsecases.IFilesUsecase
	query          string
	queryFields    []string
	lastStackIndex int
	values         []any
}

func UpdateProductBuilder(db *sqlx.DB, req *products.Product, fileUsecase filesUsecases.IFilesUsecase) IUpdateProductBuilder {
	return &updateProductBuilder{
		db:          db,
		req:         req,
		fileUsecase: fileUsecase,
		queryFields: make([]string, 0),
		values:      make([]any, 0),
	}
}

func (b *updateProductBuilder) initTransaction() error {
	txn, err := b.db.BeginTxx(context.Background(), nil)

	if err != nil {
		return err
	}

	b.txn = txn
	return nil
}

func (b *updateProductBuilder) initQuery() {
	b.query += `UPDATE "products" SET`
}

func (b *updateProductBuilder) updateTitleQuery() {
	if b.req.Title != "" {
		b.values = append(b.values, b.req.Title)
		b.lastStackIndex = len(b.values)
		b.queryFields = append(b.queryFields, fmt.Sprintf(` "title" = $%d`, b.lastStackIndex))
	}
}

func (b *updateProductBuilder) updateDescriptionQuery() {
	if b.req.Description != "" {
		b.values = append(b.values, b.req.Description)
		b.lastStackIndex = len(b.values)
		b.queryFields = append(b.queryFields, fmt.Sprintf(` "description" = $%d`, b.lastStackIndex))
	}
}

func (b *updateProductBuilder) updatePriceQuery() {
	if b.req.Price > 0 {
		b.values = append(b.values, b.req.Price)
		b.lastStackIndex = len(b.values)
		b.queryFields = append(b.queryFields, fmt.Sprintf(` "price" = $%d`, b.lastStackIndex))
	}
}

func (b *updateProductBuilder) updateCategory() error {
	if b.req.Category == nil || b.req.Category.Id <= 0 {
		return nil
	}

	query := `UPDATE "products_categories" SET "category_id" = $1 WHERE "product_id" = $2`

	if _, err := b.txn.ExecContext(context.Background(), query, b.req.Category.Id, b.req.Id); err != nil {
		b.txn.Rollback()
		return fmt.Errorf("update category failed: %v", err)
	}

	return nil
}

func (b *updateProductBuilder) insertImages() error {
	query := `INSERT INTO "images" ("filename", "url", "product_id") VALUES`
	valueStack := make([]any, 0)

	var index int

	for i := range b.req.Images {
		valueStack = append(valueStack, b.req.Images[i].FileName, b.req.Images[i].Url, b.req.Id)

		if i != len(b.req.Images)-1 {
			query += fmt.Sprintf(` ($%d, $%d, $%d),`, index+1, index+2, index+3)
		} else {
			query += fmt.Sprintf(` ($%d, $%d, $%d)`, index+1, index+2, index+3)
		}

		index += 3
	}

	if _, err := b.txn.ExecContext(context.Background(), query, valueStack...); err != nil {
		b.txn.Rollback()
		return fmt.Errorf("insert images failed: %v", err)
	}

	return nil
}

func (b *updateProductBuilder) getOldImages() []*entities.Image {
	query := `SELECT "id", "filename", "url" FROM "images" WHERE "product_id" = $1`
	images := make([]*entities.Image, 0)

	if err := b.txn.Select(context.Background(), query, b.req.Id); err != nil {
		return make([]*entities.Image, 0)
	}

	return images
}

func (b *updateProductBuilder) deleteOldImages() error {
	query := `DELETE FROM "images" WHERE "product_id" = $1`
	images := b.getOldImages()

	if len(images) > 0 {
		deleteFilesReq := make([]*files.DeleteFileReq, 0)

		for _, img := range images {
			deleteFilesReq = append(deleteFilesReq, &files.DeleteFileReq{
				Destination: fmt.Sprintf("%s/%s", b.app.ProductBucket(), img.FileName),
			})
		}

		if err := b.fileUsecase.DeleteFilesOnGCP(deleteFilesReq); err != nil {
			b.txn.Rollback()
			return err
		}
	}

	if _, err := b.txn.ExecContext(context.Background(), query, b.req.Id); err != nil {
		b.txn.Rollback()
		return fmt.Errorf("delete old images failed: %v", err)
	}

	return nil
}

func (b *updateProductBuilder) closeQuery() {
	b.values = append(b.values, b.req.Id)
	b.lastStackIndex = len(b.values)
	b.query += fmt.Sprintf(` WHERE "id" = $%d`, b.lastStackIndex)
}

func (b *updateProductBuilder) updateProduct() error {
	if _, err := b.txn.ExecContext(context.Background(), b.query, b.values...); err != nil {
		b.txn.Rollback()
		return fmt.Errorf("update product failed: %v", err)
	}

	return nil
}

func (b *updateProductBuilder) getQueryFields() []string {
	return b.queryFields
}

func (b *updateProductBuilder) getValues() []any {
	return b.values
}

func (b *updateProductBuilder) getQuery() string {
	return b.query
}

func (b *updateProductBuilder) setQuery(query string) {
	b.query = query
}

func (b *updateProductBuilder) getImagesLen() int {
	return len(b.req.Images)
}

func (b *updateProductBuilder) commit() error {
	if err := b.txn.Commit(); err != nil {
		return err
	}

	return nil
}
