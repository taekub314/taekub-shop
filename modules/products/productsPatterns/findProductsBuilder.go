package productsPatterns

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/modules/products"
	"gitlab.com/taekub314/taekub-shop/pkg/utilities"
)

type IFindProductsBuilder interface {
	initQuery()
	initCountQuery()
	builderWhereQuery()
	builderSort()
	builderPaginate()
	closeQuery()
	resetQuery()
	getQuery() string
	getValues() []any
	getDb() *sqlx.DB
	PrintQuery()
}

type findProductsBuilder struct {
	db             *sqlx.DB
	req            *products.ProductFilter
	query          string
	lastStackIndex int
	values         []any
}

func FindProductsBuilder(db *sqlx.DB, req *products.ProductFilter) IFindProductsBuilder {
	return &findProductsBuilder{
		db:  db,
		req: req,
	}
}

func (b *findProductsBuilder) PrintQuery() {
	utilities.Debug(b.values)
	fmt.Println(b.query)
}

func (b *findProductsBuilder) initQuery() {
	b.query += `
		SELECT
			array_to_json(array_agg("t"))
		FROM (
			SELECT
				"p"."id",
				"p"."title",
				"p"."description",
				"p"."price",
				(
					SELECT
						to_jsonb("ct")
					FROM (
						SELECT
							"c"."id",
							"c"."title"
						FROM "categories" "c"
						LEFT JOIN "products_categories" "pc" ON "c".id = "pc"."category_id"
						WHERE "pc"."product_id" = "p"."id"
					) AS "ct"
				) AS "category",
				"p"."created_at",
				"p"."updated_at",
				(
					SELECT
						COALESCE(array_to_json(array_agg("it")), '[]'::json)
					FROM (
						SELECT
							"i"."id",
							"i"."filename",
							"i"."url"
						FROM "images" "i"
						WHERE "i"."product_id" = "p"."id"
					) AS "it"
				) AS "images"
				FROM "products" "p"
				WHERE 1 = 1`
}

func (b *findProductsBuilder) initCountQuery() {
	b.query += `
		SELECT
			COUNT(1) AS "count"
		FROM "products" "p"
		WHERE 1 = 1`
}

func (b *findProductsBuilder) builderWhereQuery() {
	var queryWhere string
	queryWhereStack := make([]string, 0)

	// check id
	if b.req.Id != "" {
		b.values = append(b.values, b.req.Id)
		queryWhereStack = append(queryWhereStack, ` AND "p"."id" = ?`)
	}

	// check search
	if b.req.Search != "" {
		b.values = append(
			b.values,
			"%"+strings.ToLower(b.req.Search)+"%",
			"%"+strings.ToLower(b.req.Search)+"%",
		)

		queryWhereStack = append(queryWhereStack, ` AND (LOWER("title") LIKE ? OR LOWER("description") LIKE ?)`)
	}

	for i := range queryWhereStack {
		if i != len(queryWhereStack)-1 {
			queryWhere += strings.Replace(queryWhereStack[i], "?", "$"+strconv.Itoa(i+1), 1)
		} else {
			queryWhere += strings.Replace(queryWhereStack[i], "?", "$"+strconv.Itoa(i+1), 1)
			queryWhere = strings.Replace(queryWhere, "?", "$"+strconv.Itoa(i+2), 2)
		}
	}

	b.lastStackIndex = len(b.values)
	b.query += queryWhere
}

func (b *findProductsBuilder) builderSort() {
	// b.values = append(b.values, b.req.OrderBy)
	// b.query += fmt.Sprintf(` ORDER BY $%d %s`, b.lastStackIndex+1, b.req.Sort)
	// b.lastStackIndex = len(b.values)

	b.query += fmt.Sprintf(` ORDER BY %s %s`, b.req.OrderBy, b.req.Sort)
}

func (b *findProductsBuilder) builderPaginate() {
	b.values = append(b.values, (b.req.Page-1)*b.req.PageSize, b.req.PageSize)
	b.query += fmt.Sprintf(` OFFSET $%d LIMIT $%d`, b.lastStackIndex+1, b.lastStackIndex+2)
	b.lastStackIndex = len(b.values)
}

func (b *findProductsBuilder) closeQuery() {
	b.query += `) AS "t"`
}

func (b *findProductsBuilder) resetQuery() {
	b.query = ""
	b.values = make([]any, 0)
	b.lastStackIndex = 0
}

func (b *findProductsBuilder) getQuery() string {
	return b.query
}

func (b *findProductsBuilder) getValues() []any {
	return b.values
}

func (b *findProductsBuilder) getDb() *sqlx.DB {
	return b.db
}
