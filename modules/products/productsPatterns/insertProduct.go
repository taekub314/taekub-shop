package productsPatterns

type insertProductEngineer struct {
	builder IInsertProductBuilder
}

func InsertProductEngineer(builder IInsertProductBuilder) *insertProductEngineer {
	return &insertProductEngineer{builder: builder}
}

func (e *insertProductEngineer) InsertProduct() (string, error) {
	if err := e.builder.initTransaction(); err != nil {
		return "", err
	}

	if err := e.builder.insertProduct(); err != nil {
		return "", err
	}

	if err := e.builder.insertCategory(); err != nil {
		return "", err
	}

	if err := e.builder.insertAttachment(); err != nil {
		return "", err
	}

	if err := e.builder.commit(); err != nil {
		return "", err
	}

	return e.builder.getProductId(), nil
}
