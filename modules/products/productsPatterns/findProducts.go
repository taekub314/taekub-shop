package productsPatterns

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"gitlab.com/taekub314/taekub-shop/modules/products"
)

type findProductsEngineer struct {
	buider IFindProductsBuilder
}

func FindProductsEngineer(buider IFindProductsBuilder) *findProductsEngineer {
	return &findProductsEngineer{
		buider: buider,
	}
}

func (e *findProductsEngineer) FindProducts() []*products.Product {
	_, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	e.buider.initQuery()
	e.buider.builderWhereQuery()
	e.buider.builderSort()
	e.buider.builderPaginate()
	e.buider.closeQuery()

	bytes := make([]byte, 0)
	productResult := make([]*products.Product, 0)

	if err := e.buider.getDb().Get(&bytes, e.buider.getQuery(), e.buider.getValues()...); err != nil {
		log.Printf("search products failed: %v", err)
		return make([]*products.Product, 0)
	}

	if err := json.Unmarshal(bytes, &productResult); err != nil {
		log.Printf("unmarshal product result failed: %v", err)
		return make([]*products.Product, 0)
	}

	e.buider.resetQuery()
	return productResult
}

func (e *findProductsEngineer) CountProducts() int {
	_, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	e.buider.initCountQuery()
	e.buider.builderWhereQuery()

	var count int

	if err := e.buider.getDb().Get(&count, e.buider.getQuery(), e.buider.getValues()...); err != nil {
		log.Printf("count products failed: %v", err)
		return 0
	}

	e.buider.resetQuery()
	return count
}
