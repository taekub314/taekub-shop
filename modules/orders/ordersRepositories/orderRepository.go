package ordersRepositories

import (
	"context"
	"encoding/json"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/modules/orders"
	"gitlab.com/taekub314/taekub-shop/modules/orders/ordersPatterns"
)

type IOrdersRepository interface {
	FindOneOrder(orderId string) (*orders.Order, error)
	FindOrders(req *orders.OrderFilter) ([]*orders.Order, int)
	InsertOrder(req *orders.Order) (string, error)
	UpdateOrder(req *orders.Order) error
}

type ordersRepository struct {
	db *sqlx.DB
}

func OrdersRepository(db *sqlx.DB) IOrdersRepository {
	return &ordersRepository{db: db}
}

func (r *ordersRepository) FindOneOrder(orderId string) (*orders.Order, error) {
	query := `
		SELECT
			to_jsonb("t")
		FROM (
			SELECT
				"o"."id",
				"o"."user_id",
				"o"."transfer_slip",
				(
					SELECT
						array_to_json(array_agg("pt"))
					FROM (
						SELECT
							"spo"."id",
							"spo"."quantity",
							"spo"."product"
						FROM "products_orders" "spo"
						WHERE "spo"."order_id" = "o"."id"
					) AS "pt"
				) AS "products",
				"o"."address",
				"o"."contact",
				(
					SELECT
						SUM(COALESCE(("po"."product"->>'price')::FLOAT * ("po"."quantity")::FLOAT, 0))
					FROM "products_orders" "po"
					WHERE "po"."order_id" = "o"."id"
				) AS "total_paid",
				"o"."status",
				"o"."created_at",
				"o"."updated_at"
			FROM "orders" "o"
			WHERE "o"."id" = $1
		) AS "t"`

	orderByte := make([]byte, 0)
	order := &orders.Order{
		Products: make([]*orders.ProductsOrder, 0),
	}

	if err := r.db.Get(&orderByte, query, orderId); err != nil {
		return nil, fmt.Errorf("get order failed: %v", err)
	}

	if err := json.Unmarshal(orderByte, &order); err != nil {
		return nil, fmt.Errorf("unmarshal order failed: %v", err)
	}

	return order, nil
}

func (r *ordersRepository) FindOrders(req *orders.OrderFilter) ([]*orders.Order, int) {
	builder := ordersPatterns.FindOrdersBuilder(r.db, req)
	engineer := ordersPatterns.FindOrdersEngineer(builder)
	result := engineer.FindOrders()
	count := engineer.CountOrders()

	return result, count
}

func (r *ordersRepository) InsertOrder(req *orders.Order) (string, error) {
	builder := ordersPatterns.InsertOrderBuilder(r.db, req)
	engineer := ordersPatterns.InsertOrderEngineer(builder)
	orderId, err := engineer.InsertOrder()

	if err != nil {
		return "", err
	}

	return orderId, err
}

func (r *ordersRepository) UpdateOrder(req *orders.Order) error {
	query := `UPDATE "orders" SET`

	queryWhereStack := make([]string, 0)
	values := make([]any, 0)
	lastIndex := 1

	if req.Status != "" {
		values = append(values, req.Status)
		queryWhereStack = append(queryWhereStack, fmt.Sprintf(` "status" = $%d?`, lastIndex))
		lastIndex++
	}

	if req.TransferSlip != nil {
		values = append(values, req.TransferSlip)
		queryWhereStack = append(queryWhereStack, fmt.Sprintf(` "transfer_slip" = $%d?`, lastIndex))
		lastIndex++
	}

	for i, v := range queryWhereStack {
		if i != len(queryWhereStack)-1 {
			query += strings.Replace(v, "?", ",", 1)
		} else {
			query += strings.Replace(v, "?", "", 1)
		}
	}

	values = append(values, req.Id)
	query += fmt.Sprintf(` WHERE "id" = $%d`, lastIndex)

	if _, err := r.db.ExecContext(context.Background(), query, values...); err != nil {
		return fmt.Errorf("update order failed: %v", err)
	}

	return nil
}
