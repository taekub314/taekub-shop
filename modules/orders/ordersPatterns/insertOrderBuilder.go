package ordersPatterns

import (
	"context"
	"fmt"
	"time"

	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/modules/orders"
)

type IInsertOrderBuilder interface {
	initTransaction() error
	insertOrder() error
	insertProductsOrder() error
	commit() error
	getOrderId() string
}

type insertOrderBuilder struct {
	db  *sqlx.DB
	txn *sqlx.Tx
	req *orders.Order
}

func InsertOrderBuilder(db *sqlx.DB, req *orders.Order) IInsertOrderBuilder {
	return &insertOrderBuilder{
		db:  db,
		req: req,
	}
}

func (b *insertOrderBuilder) initTransaction() error {
	txn, err := b.db.BeginTxx(context.Background(), nil)

	if err != nil {
		return err
	}

	b.txn = txn
	return nil
}

func (b *insertOrderBuilder) insertOrder() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	query := `INSERT INTO "orders" ("user_id", "contact", "address", "transfer_slip", "status") VALUES ($1, $2, $3, $4, $5) RETURNING "id"`

	if err := b.txn.QueryRowxContext(ctx, query, b.req.UserId, b.req.Contact, b.req.Address, b.req.TransferSlip, b.req.Status).Scan(&b.req.Id); err != nil {
		b.txn.Rollback()
		return fmt.Errorf("insert order failed: %v", err)
	}

	return nil
}

func (b *insertOrderBuilder) insertProductsOrder() error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	query := `
		INSERT INTO "products_orders" (
			"order_id",
			"quantity",
			"product"
		) VALUES`

	values := make([]any, 0)
	lastIndex := 0

	for i, v := range b.req.Products {
		values = append(values, b.req.Id, v.Quantity, v.Product)

		if i != len(b.req.Products)-1 {
			query += fmt.Sprintf(` ($%d, $%d, $%d),`, lastIndex+1, lastIndex+2, lastIndex+3)
		} else {
			query += fmt.Sprintf(` ($%d, $%d, $%d)`, lastIndex+1, lastIndex+2, lastIndex+3)
		}

		lastIndex += 3
	}

	if _, err := b.txn.ExecContext(ctx, query, values...); err != nil {
		b.txn.Rollback()
		return fmt.Errorf("insert product order failed: %v", err)
	}

	return nil
}

func (b *insertOrderBuilder) commit() error {
	if err := b.txn.Commit(); err != nil {
		return err
	}

	return nil
}

func (b *insertOrderBuilder) getOrderId() string {
	return b.req.Id
}
