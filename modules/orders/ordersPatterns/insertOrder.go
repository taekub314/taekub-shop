package ordersPatterns

type insertOrderEngineer struct {
	builder IInsertOrderBuilder
}

func InsertOrderEngineer(builder IInsertOrderBuilder) *insertOrderEngineer {
	return &insertOrderEngineer{builder: builder}
}

func (e *insertOrderEngineer) InsertOrder() (string, error) {
	if err := e.builder.initTransaction(); err != nil {
		return "", err
	}

	if err := e.builder.insertOrder(); err != nil {
		return "", err
	}

	if err := e.builder.insertProductsOrder(); err != nil {
		return "", err
	}

	if err := e.builder.commit(); err != nil {
		return "", err
	}

	return e.builder.getOrderId(), nil
}
