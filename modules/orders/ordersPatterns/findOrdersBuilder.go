package ordersPatterns

import (
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/modules/orders"
)

type IFindOrdersBuilder interface {
	initQuery()
	initCountQuery()
	builderWhereSearch()
	builderWhereStatus()
	builderWhereDate()
	builderSort()
	builderPaginate()
	closeQuery()
	resetQuery()
	getQuery() string
	getValues() []any
	getDb() *sqlx.DB
}

type findOrdersBuilder struct {
	db        *sqlx.DB
	req       *orders.OrderFilter
	query     string
	values    []any
	lastIndex int
}

func FindOrdersBuilder(db *sqlx.DB, req *orders.OrderFilter) IFindOrdersBuilder {
	return &findOrdersBuilder{
		db:     db,
		req:    req,
		values: make([]any, 0),
	}
}

func (b *findOrdersBuilder) initQuery() {
	b.query += `
		SELECT
			array_to_json(array_agg("t"))
		FROM (
			SELECT
				"o"."id",
				"o"."user_id",
				"o"."transfer_slip",
				(
					SELECT
						array_to_json(array_agg("pt"))
					FROM (
						SELECT
							"spo"."id",
							"spo"."quantity",
							"spo"."product"
						FROM "products_orders" "spo"
						WHERE "spo"."order_id" = "o"."id"
					) AS "pt"
				) AS "products",
				"o"."address",
				"o"."contact",
				(
					SELECT
						SUM(COALESCE(("po"."product"->>'price')::FLOAT * ("po"."quantity")::FLOAT, 0))
					FROM "products_orders" "po"
					WHERE "po"."order_id" = "o"."id"
				) AS "total_paid",
				"o"."status",
				"o"."created_at",
				"o"."updated_at"
			FROM "orders" "o"
			WHERE 1 = 1`
}

func (b *findOrdersBuilder) initCountQuery() {
	b.query += `
		SELECT
			COUNT(1) AS "count"
		FROM "orders" "o"
		WHERE 1 = 1`
}

func (b *findOrdersBuilder) builderWhereSearch() {
	if b.req.Search != "" {
		b.values = append(
			b.values,
			"%"+strings.ToLower(b.req.Search)+"%",
			"%"+strings.ToLower(b.req.Search)+"%",
			"%"+strings.ToLower(b.req.Search)+"%",
		)

		query := fmt.Sprintf(` AND (
				LOWER("o"."user_id") LIKE $%d OR
				LOWER("o"."address") LIKE $%d OR
				LOWER("o"."contact") LIKE $%d
			)`,
			b.lastIndex+1,
			b.lastIndex+2,
			b.lastIndex+3,
		)

		b.query += query
		b.lastIndex = len(b.values)
	}
}

func (b *findOrdersBuilder) builderWhereStatus() {
	if b.req.Status != "" {
		b.values = append(b.values, strings.ToLower(b.req.Status))
		b.query += fmt.Sprintf(` AND "o"."status" = $%d`, b.lastIndex+1)
		b.lastIndex = len(b.values)
	}
}

func (b *findOrdersBuilder) builderWhereDate() {
	if b.req.StartDate != "" && b.req.EndDate != "" {
		b.values = append(b.values, b.req.StartDate, b.req.EndDate)
		b.query += fmt.Sprintf(` AND "o"."created_at" BETWEEN DATE($%d) AND ($%d)::DATE + 1`, b.lastIndex+1, b.lastIndex+2)
		b.lastIndex = len(b.values)
	}
}

func (b *findOrdersBuilder) builderSort() {
	b.query += fmt.Sprintf(` ORDER BY %s %s`, b.req.OrderBy, b.req.Sort)
}

func (b *findOrdersBuilder) builderPaginate() {
	b.values = append(b.values, (b.req.Page-1)*b.req.PageSize, b.req.PageSize)
	b.query += fmt.Sprintf(` OFFSET $%d LIMIT $%d`, b.lastIndex+1, b.lastIndex+2)
	b.lastIndex = len(b.values)
}

func (b *findOrdersBuilder) closeQuery() {
	b.query += ` ) AS "t"`
}

func (b *findOrdersBuilder) resetQuery() {
	b.query = ""
	b.values = make([]any, 0)
	b.lastIndex = 0
}

func (b *findOrdersBuilder) getQuery() string {
	return b.query
}

func (b *findOrdersBuilder) getValues() []any {
	return b.values
}

func (b *findOrdersBuilder) getDb() *sqlx.DB {
	return b.db
}
