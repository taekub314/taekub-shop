package ordersPatterns

import (
	"context"
	"encoding/json"
	"log"
	"time"

	"gitlab.com/taekub314/taekub-shop/modules/orders"
)

type findOrdersEngineer struct {
	builder IFindOrdersBuilder
}

func FindOrdersEngineer(builder IFindOrdersBuilder) *findOrdersEngineer {
	return &findOrdersEngineer{builder: builder}
}

func (e *findOrdersEngineer) FindOrders() []*orders.Order {
	_, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	e.builder.initQuery()
	e.builder.builderWhereSearch()
	e.builder.builderWhereStatus()
	e.builder.builderWhereDate()
	e.builder.builderSort()
	e.builder.builderPaginate()
	e.builder.closeQuery()

	bytes := make([]byte, 0)
	orderResult := make([]*orders.Order, 0)

	if err := e.builder.getDb().Get(&bytes, e.builder.getQuery(), e.builder.getValues()...); err != nil {
		log.Printf("search orders failed: %v", err)
		return make([]*orders.Order, 0)
	}

	if err := json.Unmarshal(bytes, &orderResult); err != nil {
		log.Printf("unmarshal orders result failed: %v", err)
		return make([]*orders.Order, 0)
	}

	e.builder.resetQuery()
	return orderResult
}

func (e *findOrdersEngineer) CountOrders() int {
	_, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	e.builder.initCountQuery()
	e.builder.builderWhereSearch()
	e.builder.builderWhereStatus()
	e.builder.builderWhereDate()

	var count int

	if err := e.builder.getDb().Get(&count, e.builder.getQuery(), e.builder.getValues()...); err != nil {
		log.Printf("count orders failed: %v", err)
		return 0
	}

	e.builder.resetQuery()
	return count
}
