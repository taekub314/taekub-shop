package ordersHandlers

import (
	"fmt"
	"strings"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/orders"
	"gitlab.com/taekub314/taekub-shop/modules/orders/ordersUsecases"
)

type orderHandlerErrCode string

const (
	findOneOrderErr orderHandlerErrCode = "order-001"
	findOrdersErr   orderHandlerErrCode = "order-002"
	addOrderErr     orderHandlerErrCode = "order-003"
	updateOrderErr  orderHandlerErrCode = "order-004"
)

type IOrdersHandler interface {
	FindOneOrder(ctx *fiber.Ctx) error
	FindOrders(ctx *fiber.Ctx) error
	AddOrder(ctx *fiber.Ctx) error
	UpdateOrder(ctx *fiber.Ctx) error
}

type ordersHandler struct {
	conf          config.IConfig
	ordersUsecase ordersUsecases.IOrdersUsecase
}

func OrdersHandler(conf config.IConfig, ordersUsecase ordersUsecases.IOrdersUsecase) IOrdersHandler {
	return &ordersHandler{
		conf:          conf,
		ordersUsecase: ordersUsecase,
	}
}

func (h *ordersHandler) FindOneOrder(ctx *fiber.Ctx) error {
	orderId := strings.Trim(ctx.Params("order_id", ""), " ")

	if orderId == "" {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(findOneOrderErr),
			"invalid order id",
		).Resp()
	}

	order, err := h.ordersUsecase.FindOneOrder(orderId)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(findOneOrderErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, order).Resp()
}

func (h *ordersHandler) FindOrders(ctx *fiber.Ctx) error {
	req := &orders.OrderFilter{
		PaginationReq: &entities.PaginationReq{},
		SortReq:       &entities.SortReq{},
	}

	if err := ctx.QueryParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(findOrdersErr),
			"invalid criteria search",
		).Resp()
	}

	if err := h.validateDate(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(findOrdersErr),
			err.Error(),
		).Resp()
	}

	h.initialSort(req)
	h.initialPagination(req)

	return entities.NewResponse(ctx).Success(fiber.StatusOK, h.ordersUsecase.FindOrders(req)).Resp()
}

func (h *ordersHandler) validateDate(req *orders.OrderFilter) error {
	if req.StartDate != "" {
		if _, err := time.Parse("2006-01-02", req.StartDate); err != nil {
			return fmt.Errorf("invalid start date")
		}
	}

	if req.EndDate != "" {
		if _, err := time.Parse("2006-01-02", req.EndDate); err != nil {
			return fmt.Errorf("invalid end date")
		}
	}

	return nil
}

func (h *ordersHandler) initialSort(req *orders.OrderFilter) {
	orderBy := map[string]string{
		"id":         `"o"."id"`,
		"created_at": `"o"."created_at"`,
	}

	if orderBy[req.OrderBy] == "" {
		req.OrderBy = orderBy["id"]
	} else {
		req.OrderBy = orderBy[req.OrderBy]
	}

	sort := map[string]string{
		"ASC":  "ASC",
		"DESC": "DESC",
	}

	if sort[strings.ToUpper(req.Sort)] == "" {
		req.Sort = sort["DESC"]
	} else {
		req.Sort = sort[strings.ToUpper(req.Sort)]
	}
}

func (h *ordersHandler) initialPagination(req *orders.OrderFilter) {
	if req.Page < 1 {
		req.Page = 1
	}

	if req.PageSize < 5 {
		req.PageSize = 5
	}
}

func (h *ordersHandler) AddOrder(ctx *fiber.Ctx) error {
	userId := ctx.Locals("userId").(string)
	req := &orders.Order{
		Products: make([]*orders.ProductsOrder, 0),
	}

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(addOrderErr),
			err.Error(),
		).Resp()
	}

	if len(req.Products) < 1 {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(addOrderErr),
			"product are empty",
		).Resp()
	}

	if ctx.Locals("roleId").(int) != 2 {
		req.UserId = userId
	}

	req.Status = "waiting"
	req.TotalPaid = 0
	order, err := h.ordersUsecase.InsertOrder(req)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(addOrderErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusCreated, order).Resp()
}

func (h *ordersHandler) UpdateOrder(ctx *fiber.Ctx) error {
	orderId := strings.Trim(ctx.Params("order_id"), " ")
	req := new(orders.Order)

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(updateOrderErr),
			err.Error(),
		).Resp()
	}

	req.Id = orderId

	statusMap := map[string]string{
		"waiting":   "waiting",
		"shiping":   "shiping",
		"completed": "completed",
		"canceled":  "canceled",
	}

	if ctx.Locals("roleId").(int) == 2 {
		req.Status = statusMap[strings.ToLower(req.Status)]
	} else if strings.ToLower(req.Status) == statusMap["canceled"] {
		req.Status = statusMap["canceled"]
	}

	if req.TransferSlip != nil {
		if req.TransferSlip.Id == "" {
			req.TransferSlip.Id = uuid.NewString()
		}

		if req.TransferSlip.CreatedAt == "" {
			loc, err := time.LoadLocation("Asia/Bangkok")

			if err != nil {
				return entities.NewResponse(ctx).Error(
					fiber.ErrBadRequest.Code,
					string(updateOrderErr),
					err.Error(),
				).Resp()
			}

			now := time.Now().In(loc)
			req.TransferSlip.CreatedAt = now.Format("2006-01-02 15:04:05")
		}
	}

	order, err := h.ordersUsecase.UpdateOrder(req)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(updateOrderErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, order).Resp()
}
