package ordersUsecases

import (
	"fmt"
	"math"

	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/orders"
	"gitlab.com/taekub314/taekub-shop/modules/orders/ordersRepositories"
	"gitlab.com/taekub314/taekub-shop/modules/products/productsRepositories"
)

type IOrdersUsecase interface {
	FindOneOrder(orderId string) (*orders.Order, error)
	FindOrders(req *orders.OrderFilter) *entities.PaginationRes
	InsertOrder(req *orders.Order) (*orders.Order, error)
	UpdateOrder(req *orders.Order) (*orders.Order, error)
}

type ordersUsecase struct {
	ordersRepository   ordersRepositories.IOrdersRepository
	productsRepository productsRepositories.IProductsRepository
}

func OrdersUsecase(ordersRepository ordersRepositories.IOrdersRepository, productsRepository productsRepositories.IProductsRepository) IOrdersUsecase {
	return &ordersUsecase{
		ordersRepository:   ordersRepository,
		productsRepository: productsRepository,
	}
}

func (u *ordersUsecase) FindOneOrder(orderId string) (*orders.Order, error) {
	order, err := u.ordersRepository.FindOneOrder(orderId)

	if err != nil {
		return nil, err
	}

	return order, nil
}

func (u *ordersUsecase) FindOrders(req *orders.OrderFilter) *entities.PaginationRes {
	result, count := u.ordersRepository.FindOrders(req)

	return &entities.PaginationRes{
		Data:      result,
		Page:      req.Page,
		PageSize:  req.PageSize,
		TotalItem: count,
		TotalPage: int(math.Ceil(float64(count) / float64(req.PageSize))),
	}
}

func (u *ordersUsecase) InsertOrder(req *orders.Order) (*orders.Order, error) {
	for _, v := range req.Products {
		if v.Product == nil {
			return nil, fmt.Errorf("product are empty")
		}

		product, err := u.productsRepository.FindOneProduct(v.Product.Id)

		if err != nil {
			return nil, err
		}

		req.TotalPaid += v.Product.Price * float64(v.Quantity)
		v.Product = product
	}

	orderId, err := u.ordersRepository.InsertOrder(req)

	if err != nil {
		return nil, err
	}

	order, err := u.ordersRepository.FindOneOrder(orderId)

	if err != nil {
		return nil, err
	}

	return order, nil
}

func (u *ordersUsecase) UpdateOrder(req *orders.Order) (*orders.Order, error) {
	if err := u.ordersRepository.UpdateOrder(req); err != nil {
		return nil, err
	}

	order, err := u.ordersRepository.FindOneOrder(req.Id)
	return order, err
}
