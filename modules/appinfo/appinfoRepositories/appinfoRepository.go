package appinfoRepositories

import (
	"context"
	"fmt"
	"strings"

	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/modules/appinfo"
)

type IAppinfoRepository interface {
	FindCategory(req *appinfo.CategoryFilter) ([]*appinfo.Category, error)
	InsertCategory([]*appinfo.Category) error
	DeleteCategory(categoryId int) error
}

type appinfoRepository struct {
	db *sqlx.DB
}

func AppinfoRepository(db *sqlx.DB) IAppinfoRepository {
	return &appinfoRepository{db: db}
}

func (r *appinfoRepository) FindCategory(req *appinfo.CategoryFilter) ([]*appinfo.Category, error) {
	query := `SELECT "id", "title" FROM "categories"`
	filterValue := make([]any, 0)

	if req.Title != "" {
		query += ` WHERE (LOWER("title") LIKE $1)`
		filterValue = append(filterValue, "%"+strings.ToLower(req.Title)+"%")
	}

	categories := make([]*appinfo.Category, 0)

	if err := r.db.Select(&categories, query, filterValue...); err != nil {
		return nil, fmt.Errorf("select categories failed: %v", err)
	}

	return categories, nil
}

func (r *appinfoRepository) InsertCategory(req []*appinfo.Category) error {
	ctx := context.Background()
	query := `INSERT INTO "categories" ("title") VALUES`
	txn, err := r.db.BeginTxx(ctx, nil)

	if err != nil {
		return err
	}

	valueStack := make([]any, 0)

	for i, cat := range req {
		valueStack = append(valueStack, cat.Title)

		if i != len(req)-1 {
			query += fmt.Sprintf(` ($%d),`, i+1)
		} else {
			query += fmt.Sprintf(` ($%d)`, i+1)
		}
	}

	query += ` RETURNING "id"`

	rows, err := txn.QueryxContext(ctx, query, valueStack...)

	if err != nil {
		txn.Rollback()
		return fmt.Errorf("insert categories failed: %v", err)
	}

	var i int = 0
	for rows.Next() {
		if err := rows.Scan(&req[i].Id); err != nil {
			return fmt.Errorf("scan categories id failed: %v", err)
		}

		i++
	}

	if err := txn.Commit(); err != nil {
		txn.Rollback()
		return err
	}

	return nil
}

func (r *appinfoRepository) DeleteCategory(categoryId int) error {
	ctx := context.Background()
	query := `DELETE FROM "categories" WHERE id = $1`

	if _, err := r.db.ExecContext(ctx, query, categoryId); err != nil {
		return fmt.Errorf("delete category failed: %v", err)
	}

	return nil
}
