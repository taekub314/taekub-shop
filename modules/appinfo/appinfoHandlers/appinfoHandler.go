package appinfoHandlers

import (
	"strconv"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/appinfo"
	"gitlab.com/taekub314/taekub-shop/modules/appinfo/appinfoUsecases"
	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/pkg/authenticate"
)

type appinfoHandlerErrCode string

const (
	generateApiKeyErr appinfoHandlerErrCode = "appinfo-001"
	findCategoryErr   appinfoHandlerErrCode = "appinfo-002"
	addCategoriesErr  appinfoHandlerErrCode = "appinfo-003"
	removeCategoryErr appinfoHandlerErrCode = "appinfo-004"
)

type IAppinfoHandler interface {
	GenerateApiKey(ctx *fiber.Ctx) error
	FindCategory(ctx *fiber.Ctx) error
	AddCategories(ctx *fiber.Ctx) error
	RemoveCategory(ctx *fiber.Ctx) error
}

type appinfoHandler struct {
	conf           config.IConfig
	appinfoUsecase appinfoUsecases.IAppinfoUsecase
}

func AppinfoHandler(conf config.IConfig, appinfoUsecase appinfoUsecases.IAppinfoUsecase) IAppinfoHandler {
	return &appinfoHandler{
		conf:           conf,
		appinfoUsecase: appinfoUsecase,
	}
}

func (h *appinfoHandler) GenerateApiKey(ctx *fiber.Ctx) error {
	apiKey, err := authenticate.NewAuthenticate(authenticate.ApiKey, h.conf.Jwt(), nil)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(generateApiKeyErr),
			err.Error(),
		).Resp()
	}

	ctx.Locals("apiKey", apiKey)

	return entities.NewResponse(ctx).Success(
		fiber.StatusOK,
		&struct {
			Key string `json:"key"`
		}{
			Key: apiKey.SignToken(),
		},
	).Resp()
}

func (h *appinfoHandler) FindCategory(ctx *fiber.Ctx) error {
	req := new(appinfo.CategoryFilter)

	if err := ctx.QueryParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(findCategoryErr),
			err.Error(),
		).Resp()
	}

	categories, err := h.appinfoUsecase.FindCategory(req)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(findCategoryErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, categories).Resp()
}

func (h *appinfoHandler) AddCategories(ctx *fiber.Ctx) error {
	req := make([]*appinfo.Category, 0)

	if err := ctx.BodyParser(&req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(addCategoriesErr),
			err.Error(),
		).Resp()
	}

	if len(req) < 1 {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(addCategoriesErr),
			"request body are empty",
		).Resp()
	}

	if err := h.appinfoUsecase.InsertCategory(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(addCategoriesErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, req).Resp()
}

func (h *appinfoHandler) RemoveCategory(ctx *fiber.Ctx) error {
	categoryId, err := strconv.Atoi(strings.Trim(ctx.Params("category_id"), " "))

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(removeCategoryErr),
			err.Error(),
		).Resp()
	}

	if categoryId < 1 {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(removeCategoryErr),
			"invalid category id",
		).Resp()
	}

	if err := h.appinfoUsecase.DeleteCategory(categoryId); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(removeCategoryErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(
		fiber.StatusOK,
		&struct {
			CategoryId int `json:"category_id"`
		}{
			CategoryId: categoryId,
		}).Resp()
}
