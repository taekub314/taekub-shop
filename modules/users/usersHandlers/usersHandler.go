package usersHandlers

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/users"
	"gitlab.com/taekub314/taekub-shop/modules/users/usersUsecases"
	"gitlab.com/taekub314/taekub-shop/pkg/authenticate"
)

type usersHandlerErrCode string

const (
	signUpCustomerError     usersHandlerErrCode = "users-001"
	signInError             usersHandlerErrCode = "users-002"
	refreshPassportError    usersHandlerErrCode = "users-003"
	signOutError            usersHandlerErrCode = "users-004"
	signUpAdminError        usersHandlerErrCode = "users-005"
	generateAdminTokenError usersHandlerErrCode = "users-006"
	getUserProfileError     usersHandlerErrCode = "users-007"
)

type IUsersHandler interface {
	SignUpCustomer(ctx *fiber.Ctx) error
	SignUpAdmin(ctx *fiber.Ctx) error
	SignIn(ctx *fiber.Ctx) error
	RefreshPassport(ctx *fiber.Ctx) error
	SignOut(ctx *fiber.Ctx) error
	GenerateAdminToken(ctx *fiber.Ctx) error
	GetUserProfile(ctx *fiber.Ctx) error
}

type usersHandler struct {
	conf         config.IConfig
	usersUsecase usersUsecases.IUsersUsecase
}

func UsersHandler(conf config.IConfig, usersUsecase usersUsecases.IUsersUsecase) IUsersHandler {
	return &usersHandler{
		conf:         conf,
		usersUsecase: usersUsecase,
	}
}

func (h *usersHandler) GenerateAdminToken(ctx *fiber.Ctx) error {
	adminToken, err := authenticate.NewAuthenticate(
		authenticate.Admin,
		h.conf.Jwt(),
		nil,
	)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(generateAdminTokenError),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(
		fiber.StatusOK,
		&struct {
			Token string `json:"token"`
		}{
			Token: adminToken.SignToken(),
		}).Resp()
}

func (h *usersHandler) SignUpCustomer(ctx *fiber.Ctx) error {
	// Request body parser
	req := new(users.UserRegisterReq)

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(signUpCustomerError),
			err.Error(),
		).Resp()
	}

	// Email validation
	if !req.IsEmail() {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(signUpCustomerError),
			"Invalid email pattern",
		).Resp()
	}

	// Insert
	result, err := h.usersUsecase.InsertCustomer(req)

	if err != nil {
		switch err.Error() {
		case "username has been used":
		case "email has been used":
			return entities.NewResponse(ctx).Error(
				fiber.ErrBadRequest.Code,
				string(signUpCustomerError),
				err.Error(),
			).Resp()
		default:
			return entities.NewResponse(ctx).Error(
				fiber.ErrInternalServerError.Code,
				string(signUpCustomerError),
				err.Error(),
			).Resp()
		}
	}

	return entities.NewResponse(ctx).Success(fiber.StatusCreated, result).Resp()
}

func (h *usersHandler) SignUpAdmin(ctx *fiber.Ctx) error {
	// Request body parser
	req := new(users.UserRegisterReq)

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(signUpAdminError),
			err.Error(),
		).Resp()
	}

	// Email validation
	if !req.IsEmail() {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(signUpAdminError),
			"Invalid email pattern",
		).Resp()
	}

	// Insert
	result, err := h.usersUsecase.InsertAdmin(req)

	if err != nil {
		switch err.Error() {
		case "username has been used":
		case "email has been used":
			return entities.NewResponse(ctx).Error(
				fiber.ErrBadRequest.Code,
				string(signUpAdminError),
				err.Error(),
			).Resp()
		default:
			return entities.NewResponse(ctx).Error(
				fiber.ErrInternalServerError.Code,
				string(signUpAdminError),
				err.Error(),
			).Resp()
		}
	}

	return entities.NewResponse(ctx).Success(fiber.StatusCreated, result).Resp()
}

func (h *usersHandler) SignIn(ctx *fiber.Ctx) error {
	// Request body parser
	req := new(users.UserCredential)

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(signInError),
			err.Error(),
		).Resp()
	}

	result, err := h.usersUsecase.GetPassport(req)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(signInError),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, result).Resp()
}

func (h *usersHandler) RefreshPassport(ctx *fiber.Ctx) error {
	// Request body parser
	req := new(users.UserRefreshCredential)

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(refreshPassportError),
			err.Error(),
		).Resp()
	}

	result, err := h.usersUsecase.RefreshPassport(req)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(refreshPassportError),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, result).Resp()
}

func (h *usersHandler) SignOut(ctx *fiber.Ctx) error {
	// Request body parser
	req := new(users.UserRemoveCredential)

	if err := ctx.BodyParser(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(signOutError),
			err.Error(),
		).Resp()
	}

	if err := h.usersUsecase.DeleteOAuth(req.OAuthId); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(signOutError),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, nil).Resp()
}

func (h *usersHandler) GetUserProfile(ctx *fiber.Ctx) error {
	userId := strings.Trim(ctx.Params("user_id"), " ")
	result, err := h.usersUsecase.GetUserProfile(userId)

	if err != nil {
		switch err.Error() {
		case "get user failed: sql: no rows in result set":
			return entities.NewResponse(ctx).Error(
				fiber.ErrBadRequest.Code,
				string(getUserProfileError),
				err.Error(),
			).Resp()
		default:
			return entities.NewResponse(ctx).Error(
				fiber.ErrInternalServerError.Code,
				string(getUserProfileError),
				err.Error(),
			).Resp()
		}
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, result).Resp()
}
