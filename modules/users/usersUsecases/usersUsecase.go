package usersUsecases

import (
	"fmt"

	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/users"
	"gitlab.com/taekub314/taekub-shop/modules/users/usersRepositories"
	"gitlab.com/taekub314/taekub-shop/pkg/authenticate"
	"golang.org/x/crypto/bcrypt"
)

type IUsersUsecase interface {
	InsertCustomer(req *users.UserRegisterReq) (*users.UserPassport, error)
	InsertAdmin(req *users.UserRegisterReq) (*users.UserPassport, error)
	GetPassport(req *users.UserCredential) (*users.UserPassport, error)
	RefreshPassport(req *users.UserRefreshCredential) (*users.UserPassport, error)
	DeleteOAuth(oauthId string) error
	GetUserProfile(userId string) (*users.User, error)
}

type usersUsecase struct {
	conf            config.IConfig
	usersRepository usersRepositories.IUsersRepository
}

func UserUsecase(conf config.IConfig, usersRepository usersRepositories.IUsersRepository) IUsersUsecase {
	return &usersUsecase{
		conf:            conf,
		usersRepository: usersRepository,
	}
}

func (u *usersUsecase) InsertCustomer(req *users.UserRegisterReq) (*users.UserPassport, error) {
	if err := req.BcryptPassword(); err != nil {
		return nil, err
	}

	result, err := u.usersRepository.InsertUser(req, false)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (u *usersUsecase) InsertAdmin(req *users.UserRegisterReq) (*users.UserPassport, error) {
	if err := req.BcryptPassword(); err != nil {
		return nil, err
	}

	result, err := u.usersRepository.InsertUser(req, true)

	if err != nil {
		return nil, err
	}

	return result, nil
}

func (u *usersUsecase) GetPassport(req *users.UserCredential) (*users.UserPassport, error) {
	user, err := u.usersRepository.FindOneUserByEmail(req.Email)

	if err != nil {
		return nil, err
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(req.Password)); err != nil {
		return nil, fmt.Errorf("password invalid")
	}

	accessToken, _ := authenticate.NewAuthenticate(authenticate.Access, u.conf.Jwt(), &users.UserClaims{
		Id:     user.Id,
		RoleId: user.RoleId,
	})

	refreshToken, _ := authenticate.NewAuthenticate(authenticate.Refresh, u.conf.Jwt(), &users.UserClaims{
		Id:     user.Id,
		RoleId: user.RoleId,
	})

	passport := &users.UserPassport{
		User: &users.User{
			Id:       user.Id,
			Email:    user.Email,
			Username: user.Username,
			RoleId:   user.RoleId,
		},
		Token: &users.UserToken{
			AccessToken:  accessToken.SignToken(),
			RefreshToken: refreshToken.SignToken(),
		},
	}

	if err := u.usersRepository.InsertOAuth(passport); err != nil {
		return nil, err
	}

	return passport, nil
}

func (u *usersUsecase) RefreshPassport(req *users.UserRefreshCredential) (*users.UserPassport, error) {
	claims, err := authenticate.ParseToken(u.conf.Jwt(), req.RefreshToken)

	if err != nil {
		return nil, err
	}

	oauth, err := u.usersRepository.FindOneOAuth(req.RefreshToken)

	if err != nil {
		return nil, err
	}

	profile, err := u.usersRepository.GetProfile(oauth.UserId)

	if err != nil {
		return nil, err
	}

	newClaims := &users.UserClaims{
		Id:     profile.Id,
		RoleId: profile.RoleId,
	}

	accessToken, err := authenticate.NewAuthenticate(
		authenticate.Access,
		u.conf.Jwt(),
		newClaims,
	)

	if err != nil {
		return nil, err
	}

	refreshToken := authenticate.RepeatRefreshToken(
		u.conf.Jwt(),
		newClaims,
		claims.ExpiresAt.Unix(),
	)

	passport := &users.UserPassport{
		User: profile,
		Token: &users.UserToken{
			Id:           oauth.Id,
			AccessToken:  accessToken.SignToken(),
			RefreshToken: refreshToken,
		},
	}

	if err := u.usersRepository.UpdateOAuth(passport.Token); err != nil {
		return nil, err
	}

	return passport, nil
}

func (u *usersUsecase) DeleteOAuth(oauthId string) error {
	if err := u.usersRepository.DeleteOAuth(oauthId); err != nil {
		return err
	}

	return nil
}

func (u *usersUsecase) GetUserProfile(userId string) (*users.User, error) {
	profile, err := u.usersRepository.GetProfile(userId)

	if err != nil {
		return nil, err
	}

	return profile, nil
}
