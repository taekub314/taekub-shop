package middlewaresHandlers

import (
	"strings"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/middlewares/middlewaresUsecases"
	"gitlab.com/taekub314/taekub-shop/pkg/authenticate"
	"gitlab.com/taekub314/taekub-shop/pkg/utilities"
)

type middlewaresHandlerErrCode string

const (
	routerCheckError     middlewaresHandlerErrCode = "middlewares-001"
	jwtAuthenticateError middlewaresHandlerErrCode = "middlewares-002"
	paramsCheckError     middlewaresHandlerErrCode = "middlewares-003"
	authorizeError       middlewaresHandlerErrCode = "middlewares-004"
	apiKeyError          middlewaresHandlerErrCode = "middlewares-005"
)

type IMiddlewaresHandler interface {
	Cors() fiber.Handler
	RouterCheck() fiber.Handler
	Logger() fiber.Handler
	JwtAuthenticate() fiber.Handler
	ParamsCheck() fiber.Handler
	Authorize(expectRole ...int) fiber.Handler
	ApiKeyAuthenticate() fiber.Handler
}

type middlewaresHandler struct {
	conf               config.IConfig
	middlewaresUsecase middlewaresUsecases.IMiddlewaresUsecase
}

func MiddlewareHandler(conf config.IConfig, middlewaresUsecase middlewaresUsecases.IMiddlewaresUsecase) IMiddlewaresHandler {
	return &middlewaresHandler{
		conf:               conf,
		middlewaresUsecase: middlewaresUsecase,
	}
}

func (m *middlewaresHandler) Cors() fiber.Handler {
	return cors.New(cors.Config{
		Next:             cors.ConfigDefault.Next,
		AllowOrigins:     "*",
		AllowMethods:     "GET,POST,HEAD,PUT,DELETE,PATCH",
		AllowHeaders:     "",
		AllowCredentials: false,
		ExposeHeaders:    "",
		MaxAge:           0,
	})
}

func (h *middlewaresHandler) RouterCheck() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		return entities.NewResponse(ctx).Error(
			fiber.ErrNotFound.Code,
			string(routerCheckError),
			"Path not found",
		).Resp()
	}
}

func (h *middlewaresHandler) Logger() fiber.Handler {
	return logger.New(logger.Config{
		Format:     "${time} ${ip} ${status} - ${method} ${path}\n",
		TimeFormat: "02/01/2006",
		TimeZone:   "Asia/Bangkok",
	})
}

func (h *middlewaresHandler) JwtAuthenticate() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		token := strings.TrimPrefix(ctx.Get("Authorization"), "Bearer ")
		result, err := authenticate.ParseToken(h.conf.Jwt(), token)

		if err != nil {
			return entities.NewResponse(ctx).Error(
				fiber.ErrUnauthorized.Code,
				string(jwtAuthenticateError),
				err.Error(),
			).Resp()
		}

		claims := result.Claims

		if !h.middlewaresUsecase.FindAccessToken(claims.Id, token) {
			return entities.NewResponse(ctx).Error(
				fiber.ErrUnauthorized.Code,
				string(jwtAuthenticateError),
				"Unauthorized",
			).Resp()
		}

		ctx.Locals("userId", claims.Id)
		ctx.Locals("roleId", claims.RoleId)

		return ctx.Next()
	}
}

func (h *middlewaresHandler) ParamsCheck() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		userId := ctx.Locals("userId")

		if ctx.Locals("roleId").(int) == 2 {
			return ctx.Next()
		}

		if userId != ctx.Params("user_id") {
			return entities.NewResponse(ctx).Error(
				fiber.ErrForbidden.Code,
				string(paramsCheckError),
				"Forbidden",
			).Resp()
		}

		return ctx.Next()
	}
}

func (h *middlewaresHandler) Authorize(expectRole ...int) fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		roleId, ok := ctx.Locals("roleId").(int)

		if !ok {
			return entities.NewResponse(ctx).Error(
				fiber.ErrUnauthorized.Code,
				string(authorizeError),
				"role not found",
			).Resp()
		}

		roles, err := h.middlewaresUsecase.FindRole()

		if !ok {
			return entities.NewResponse(ctx).Error(
				fiber.ErrInternalServerError.Code,
				string(authorizeError),
				err.Error(),
			).Resp()
		}

		sum := 0

		for _, v := range expectRole {
			sum += v
		}

		expectValBinary := utilities.BinaryConverter(sum, len(roles))
		userValBinary := utilities.BinaryConverter(roleId, len(roles))

		for i := range userValBinary {
			if userValBinary[i]&expectValBinary[i] == 1 {
				return ctx.Next()
			}
		}

		return entities.NewResponse(ctx).Error(
			fiber.ErrForbidden.Code,
			string(authorizeError),
			"Forbidden",
		).Resp()
	}
}

func (h *middlewaresHandler) ApiKeyAuthenticate() fiber.Handler {
	return func(ctx *fiber.Ctx) error {
		key := ctx.Get("x-api-key")

		if _, err := authenticate.ParseApiKey(h.conf.Jwt(), key); err != nil {
			return entities.NewResponse(ctx).Error(
				fiber.ErrUnauthorized.Code,
				string(apiKeyError),
				err.Error(),
			).Resp()
		}

		return ctx.Next()
	}
}
