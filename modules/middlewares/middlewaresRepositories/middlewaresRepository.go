package middlewaresRepositories

import (
	"fmt"

	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/modules/middlewares"
)

type IMiddlewaresRepository interface {
	FindAccessToken(userId, accessToken string) bool
	FindRole() ([]*middlewares.Role, error)
}

type middlewaresRepository struct {
	db *sqlx.DB
}

func MiddlewaresRepository(db *sqlx.DB) IMiddlewaresRepository {
	return &middlewaresRepository{
		db: db,
	}
}

func (r *middlewaresRepository) FindAccessToken(userId, accessToken string) bool {
	query := `SELECT (CASE WHEN COUNT(1) > 0 THEN TRUE ELSE FALSE END) FROM "oauth" WHERE user_id = $1 AND access_token = $2`
	var exists bool

	if err := r.db.Get(&exists, query, userId, accessToken); err != nil {
		return false
	}

	return exists
}

func (r *middlewaresRepository) FindRole() ([]*middlewares.Role, error) {
	query := `SELECT "id", "title" FROM "roles" ORDER BY "id" DESC`
	role := make([]*middlewares.Role, 0)

	if err := r.db.Select(&role, query); err != nil {
		return nil, fmt.Errorf("select role failed: %v", err)
	}

	return role, nil
}
