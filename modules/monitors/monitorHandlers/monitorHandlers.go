package monitorHandlers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/monitors"
)

type IMonitorHandler interface {
	HealthCheck(ctx *fiber.Ctx) error
}

type monitorHandler struct {
	conf config.IConfig
}

func MonitorHandler(conf config.IConfig) IMonitorHandler {
	return &monitorHandler{
		conf: conf,
	}
}

func (hc *monitorHandler) HealthCheck(ctx *fiber.Ctx) error {
	res := &monitors.Monitor{
		Name:    hc.conf.App().Name(),
		Version: hc.conf.App().Version(),
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, res).Resp()
}
