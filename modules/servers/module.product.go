package servers

import (
	"gitlab.com/taekub314/taekub-shop/modules/products/productsHandler"
	"gitlab.com/taekub314/taekub-shop/modules/products/productsRepositories"
	"gitlab.com/taekub314/taekub-shop/modules/products/productsUsecases"
)

type IProductModule interface {
	Init()
	Repository() productsRepositories.IProductsRepository
	Usecase() productsUsecases.IProductsUsecase
	Handler() productsHandler.IProductsHandler
}

type productModule struct {
	*moduleFactory
	repository productsRepositories.IProductsRepository
	usecase    productsUsecases.IProductsUsecase
	handler    productsHandler.IProductsHandler
}

func (m *moduleFactory) ProductsModules() IProductModule {
	repository := productsRepositories.ProductsRepository(m.serv.db, m.serv.conf, m.FilesModules().Usecase())
	usecase := productsUsecases.ProductsUsecase(repository)
	handler := productsHandler.ProductsHandler(m.serv.conf, usecase, m.FilesModules().Usecase())

	return &productModule{
		moduleFactory: m,
		repository:    repository,
		usecase:       usecase,
		handler:       handler,
	}
}

func (p *productModule) Init() {
	r := p.router.Group("/products")

	r.Get("/", p.md.ApiKeyAuthenticate(), p.handler.FindProducts)
	r.Get("/:product_id", p.md.ApiKeyAuthenticate(), p.handler.FindOneProduct)

	r.Post("/", p.md.JwtAuthenticate(), p.md.Authorize(2), p.handler.AddProduct)
	r.Put("/:product_id", p.md.JwtAuthenticate(), p.md.Authorize(2), p.handler.UpdateProduct)
	r.Delete("/:product_id", p.md.JwtAuthenticate(), p.md.Authorize(2), p.handler.RemoveProduct)
}

func (p *productModule) Repository() productsRepositories.IProductsRepository {
	return p.repository
}

func (p *productModule) Usecase() productsUsecases.IProductsUsecase {
	return p.usecase
}

func (p *productModule) Handler() productsHandler.IProductsHandler {
	return p.handler
}
