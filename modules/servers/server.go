package servers

import (
	"encoding/json"
	"log"
	"os"
	"os/signal"

	"github.com/gofiber/fiber/v2"
	"github.com/jmoiron/sqlx"
	"gitlab.com/taekub314/taekub-shop/config"
)

type IServer interface {
	Start()
	GetServer() *server
}

type server struct {
	app  *fiber.App
	db   *sqlx.DB
	conf config.IConfig
}

func NewServer(db *sqlx.DB, conf config.IConfig) IServer {
	return &server{
		db:   db,
		conf: conf,
		app: fiber.New(fiber.Config{
			AppName:      conf.App().Name(),
			BodyLimit:    conf.App().BodyLimit(),
			ReadTimeout:  conf.App().ReadTimeout(),
			WriteTimeout: conf.App().WriteTimeout(),
			JSONEncoder:  json.Marshal,
			JSONDecoder:  json.Unmarshal,
		}),
	}
}

func (serv *server) Start() {
	// Middlewares
	middlewares := InitMiddlewares(serv)
	serv.app.Use(middlewares.Logger())
	serv.app.Use(middlewares.Cors())

	// Modules
	v1 := serv.app.Group("v1")

	modules := InitModule(v1, serv, middlewares)
	modules.MonitorModules()
	modules.UsersModules()
	modules.AppinfoModules()
	modules.FilesModules().Init()
	modules.ProductsModules().Init()
	modules.OrdersModules()

	serv.app.Use(middlewares.RouterCheck())

	// Graceful Shutdown
	signalServ := make(chan os.Signal, 1)
	signal.Notify(signalServ, os.Interrupt)

	go func() {
		<-signalServ

		log.Println("Server is shutting down...")
		_ = serv.app.Shutdown()
	}()

	// Listen to host:port
	log.Printf("Server is starting on %v", serv.conf.App().Url())
	serv.app.Listen(serv.conf.App().Url())
}

func (serv *server) GetServer() *server {
	return serv
}
