package servers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/taekub314/taekub-shop/modules/appinfo/appinfoHandlers"
	"gitlab.com/taekub314/taekub-shop/modules/appinfo/appinfoRepositories"
	"gitlab.com/taekub314/taekub-shop/modules/appinfo/appinfoUsecases"
	"gitlab.com/taekub314/taekub-shop/modules/middlewares/middlewaresHandlers"
	"gitlab.com/taekub314/taekub-shop/modules/middlewares/middlewaresRepositories"
	"gitlab.com/taekub314/taekub-shop/modules/middlewares/middlewaresUsecases"
	"gitlab.com/taekub314/taekub-shop/modules/monitors/monitorHandlers"
	"gitlab.com/taekub314/taekub-shop/modules/orders/ordersHandlers"
	"gitlab.com/taekub314/taekub-shop/modules/orders/ordersRepositories"
	"gitlab.com/taekub314/taekub-shop/modules/orders/ordersUsecases"
	"gitlab.com/taekub314/taekub-shop/modules/users/usersHandlers"
	"gitlab.com/taekub314/taekub-shop/modules/users/usersRepositories"
	"gitlab.com/taekub314/taekub-shop/modules/users/usersUsecases"
)

type IModuleFactory interface {
	MonitorModules()
	UsersModules()
	AppinfoModules()
	FilesModules() IFileModule
	ProductsModules() IProductModule
	OrdersModules()
}

type moduleFactory struct {
	router fiber.Router
	serv   *server
	md     middlewaresHandlers.IMiddlewaresHandler
}

func InitModule(router fiber.Router, serv *server, md middlewaresHandlers.IMiddlewaresHandler) IModuleFactory {
	return &moduleFactory{
		router: router,
		serv:   serv,
		md:     md,
	}
}

func InitMiddlewares(serv *server) middlewaresHandlers.IMiddlewaresHandler {
	repository := middlewaresRepositories.MiddlewaresRepository(serv.db)
	usecase := middlewaresUsecases.MiddlewaresUsecase(repository)

	return middlewaresHandlers.MiddlewareHandler(serv.conf, usecase)
}

func (m *moduleFactory) MonitorModules() {
	handler := monitorHandlers.MonitorHandler(m.serv.conf)

	m.router.Get("/", handler.HealthCheck)
}

func (m *moduleFactory) UsersModules() {
	repository := usersRepositories.UsersRepository(m.serv.db)
	usecase := usersUsecases.UserUsecase(m.serv.conf, repository)
	handler := usersHandlers.UsersHandler(m.serv.conf, usecase)

	r := m.router.Group("/users")

	r.Post("/signup/admin", m.md.JwtAuthenticate(), m.md.Authorize(2), handler.SignUpAdmin)
	r.Get("/secret", m.md.JwtAuthenticate(), m.md.Authorize(2), handler.GenerateAdminToken)

	r.Get("/:user_id", m.md.JwtAuthenticate(), m.md.ParamsCheck(), handler.GetUserProfile)
	r.Post("/signup", m.md.ApiKeyAuthenticate(), handler.SignUpCustomer)
	r.Post("/signin", m.md.ApiKeyAuthenticate(), handler.SignIn)
	r.Post("/signout", m.md.ApiKeyAuthenticate(), handler.SignOut)
	r.Post("/refresh", m.md.ApiKeyAuthenticate(), handler.RefreshPassport)
}

func (m *moduleFactory) AppinfoModules() {
	repository := appinfoRepositories.AppinfoRepository(m.serv.db)
	usecase := appinfoUsecases.AppinfoUsecase(repository)
	handler := appinfoHandlers.AppinfoHandler(m.serv.conf, usecase)

	r := m.router.Group("/appinfo")

	r.Get("/apikey", m.md.JwtAuthenticate(), m.md.Authorize(2), handler.GenerateApiKey)
	r.Post("/categories", m.md.JwtAuthenticate(), m.md.Authorize(2), handler.AddCategories)
	r.Delete("/categories/:category_id", m.md.JwtAuthenticate(), m.md.Authorize(2), handler.RemoveCategory)

	r.Get("/categories", m.md.ApiKeyAuthenticate(), handler.FindCategory)
}

func (m *moduleFactory) OrdersModules() {
	repository := ordersRepositories.OrdersRepository(m.serv.db)
	usecase := ordersUsecases.OrdersUsecase(repository, m.ProductsModules().Repository())
	handler := ordersHandlers.OrdersHandler(m.serv.conf, usecase)

	r := m.router.Group("/orders")

	r.Get("/", m.md.JwtAuthenticate(), m.md.Authorize(2), handler.FindOrders)
	r.Get("/:user_id/:order_id", m.md.JwtAuthenticate(), m.md.ParamsCheck(), handler.FindOneOrder)

	r.Post("/", m.md.JwtAuthenticate(), handler.AddOrder)
	r.Put("/:user_id/:order_id", m.md.JwtAuthenticate(), m.md.ParamsCheck(), handler.UpdateOrder)
}
