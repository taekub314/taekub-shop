package servers

import (
	"gitlab.com/taekub314/taekub-shop/modules/files/filesHandlers"
	"gitlab.com/taekub314/taekub-shop/modules/files/filesUsecases"
)

type IFileModule interface {
	Init()
	Usecase() filesUsecases.IFilesUsecase
	Handler() filesHandlers.IFilesHandler
}

type fileModule struct {
	*moduleFactory
	usecase filesUsecases.IFilesUsecase
	handler filesHandlers.IFilesHandler
}

func (m *moduleFactory) FilesModules() IFileModule {
	usecase := filesUsecases.FilesUsecase(m.serv.conf)
	handler := filesHandlers.FilesHandler(m.serv.conf, usecase)

	return &fileModule{
		moduleFactory: m,
		usecase:       usecase,
		handler:       handler,
	}
}

func (f *fileModule) Init() {
	r := f.router.Group("/files")

	r.Post("/upload", f.md.JwtAuthenticate(), f.md.Authorize(2), f.handler.UploadFiles)
	r.Patch("/delete", f.md.JwtAuthenticate(), f.md.Authorize(2), f.handler.DeleteFiles)
}

func (f *fileModule) Usecase() filesUsecases.IFilesUsecase {
	return f.usecase
}

func (f *fileModule) Handler() filesHandlers.IFilesHandler {
	return f.handler
}
