package filesUsecases

import (
	"bytes"
	"context"
	"fmt"
	"io"
	"time"

	"cloud.google.com/go/storage"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/files"
)

type IFilesUsecase interface {
	UploadToGCP(req []*files.FileReq) ([]*files.FileRes, error)
	DeleteFilesOnGCP(req []*files.DeleteFileReq) error
}

type filesUsecase struct {
	conf config.IConfig
}

type filesPublic struct {
	bucket      string
	destination string
	file        *files.FileRes
}

func FilesUsecase(conf config.IConfig) IFilesUsecase {
	return &filesUsecase{
		conf: conf,
	}
}

func (u *filesUsecase) UploadToGCP(req []*files.FileReq) ([]*files.FileRes, error) {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	client, err := storage.NewClient(ctx)

	if err != nil {
		return nil, fmt.Errorf("storage.NewClient: %v", err)
	}

	defer client.Close()

	jobsCh := make(chan *files.FileReq, len(req))
	resultsCh := make(chan *files.FileRes, len(req))
	errorsCh := make(chan error, len(req))

	results := make([]*files.FileRes, 0)

	// bind req[] to jobs channel
	for _, v := range req {
		jobsCh <- v
	}

	close(jobsCh)

	workers := 5

	// set worker
	for i := 0; i < workers; i++ {
		go u.streamFileUploadWorker(ctx, client, jobsCh, resultsCh, errorsCh)
	}

	// receive result from resultsCh/errorsCh channel
	for i := 0; i < len(req); i++ {
		err := <-errorsCh

		if err != nil {
			return nil, err
		}

		result := <-resultsCh
		results = append(results, result)
	}

	return results, nil
}

// streamFileUpload uploads an object via a stream.
func (u *filesUsecase) streamFileUploadWorker(ctx context.Context, client *storage.Client, jobsCh <-chan *files.FileReq, resultsCh chan<- *files.FileRes, errorsCh chan<- error) {
	bucket := u.conf.App().GCPBucket()
	for job := range jobsCh {
		container, err := job.File.Open()

		if err != nil {
			errorsCh <- err
			return
		}

		// read files to byte[]
		b, err := io.ReadAll(container)

		if err != nil {
			errorsCh <- err
			return
		}

		// convert byte[] to buffer
		buf := bytes.NewBuffer(b)
		destination := u.conf.App().ProductBucket() + "/" + job.FileName

		// Upload an object with storage.Writer.
		wc := client.Bucket(bucket).Object(destination).NewWriter(ctx)

		if _, err = io.Copy(wc, buf); err != nil {
			errorsCh <- fmt.Errorf("io.Copy: %v", err)
			return
		}

		// Data can continue to be added to the file until the writer is closed.
		if err := wc.Close(); err != nil {
			errorsCh <- fmt.Errorf("Writer.Close: %v", err)
			return
		}

		fmt.Printf("%v uploaded to %v.\n", job.FileName, destination)

		newFiles := &filesPublic{
			bucket:      bucket,
			destination: destination,
			file: &files.FileRes{
				FileName: job.FileName,
				Url:      fmt.Sprintf("https://storage.googleapis.com/%s/%s", bucket, destination),
			},
		}

		if err := newFiles.makePublic(ctx, client); err != nil {
			errorsCh <- err
			return
		}

		errorsCh <- nil
		resultsCh <- newFiles.file
	}
}

// makePublic gives all users read access to an object.
func (f *filesPublic) makePublic(ctx context.Context, client *storage.Client) error {
	acl := client.Bucket(f.bucket).Object(f.destination).ACL()

	if err := acl.Set(ctx, storage.AllUsers, storage.RoleReader); err != nil {
		return fmt.Errorf("ACLHandle.Set: %w", err)
	}

	fmt.Printf("Blob %v is now publicly accessible.\n", f.destination)
	return nil
}

func (u *filesUsecase) DeleteFilesOnGCP(req []*files.DeleteFileReq) error {
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*60)
	defer cancel()

	client, err := storage.NewClient(ctx)

	if err != nil {
		return fmt.Errorf("storage.NewClient: %v", err)
	}

	defer client.Close()

	jobsCh := make(chan *files.DeleteFileReq, len(req))
	errorsCh := make(chan error, len(req))

	// bind req[] to jobs channel
	for _, v := range req {
		jobsCh <- v
	}

	close(jobsCh)

	workers := 5

	// set worker
	for i := 0; i < workers; i++ {
		go u.deleteFileWorker(ctx, client, jobsCh, errorsCh)
	}

	// receive result from errorsCh channel
	for i := 0; i < len(req); i++ {
		err := <-errorsCh

		if err != nil {
			return err
		}
	}

	return nil
}

// deleteFile removes specified object.
func (u *filesUsecase) deleteFileWorker(ctx context.Context, client *storage.Client, jobsCh <-chan *files.DeleteFileReq, errorsCh chan<- error) {
	for job := range jobsCh {
		o := client.Bucket(u.conf.App().GCPBucket()).Object(job.Destination)

		// Optional: set a generation-match precondition to avoid potential race
		// conditions and data corruptions. The request to delete the file is aborted
		// if the object's generation number does not match your precondition.
		attrs, err := o.Attrs(ctx)

		if err != nil {
			errorsCh <- fmt.Errorf("object.Attrs: %v", err)
			return
		}

		o = o.If(storage.Conditions{GenerationMatch: attrs.Generation})

		if err := o.Delete(ctx); err != nil {
			errorsCh <- fmt.Errorf("Object(%q).Delete: %v", job.Destination, err)
			return
		}

		fmt.Printf("Blob %v deleted.\n", job.Destination)
		errorsCh <- nil
	}
}
