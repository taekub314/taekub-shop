package filesHandlers

import (
	"fmt"
	"math"
	"path/filepath"
	"strings"

	"github.com/gofiber/fiber/v2"
	"gitlab.com/taekub314/taekub-shop/config"
	"gitlab.com/taekub314/taekub-shop/modules/entities"
	"gitlab.com/taekub314/taekub-shop/modules/files"
	"gitlab.com/taekub314/taekub-shop/modules/files/filesUsecases"
	"gitlab.com/taekub314/taekub-shop/pkg/utilities"
)

type filesHandlerErrCode string

const (
	uploadFilesErr filesHandlerErrCode = "files-001"
	deleteFilesErr filesHandlerErrCode = "files-002"
)

type IFilesHandler interface {
	UploadFiles(ctx *fiber.Ctx) error
	DeleteFiles(ctx *fiber.Ctx) error
}

type filesHandler struct {
	conf         config.IConfig
	filesUsecase filesUsecases.IFilesUsecase
}

func FilesHandler(conf config.IConfig, filesUsecase filesUsecases.IFilesUsecase) IFilesHandler {
	return &filesHandler{
		conf:         conf,
		filesUsecase: filesUsecase,
	}
}

func (h *filesHandler) UploadFiles(ctx *fiber.Ctx) error {
	req := make([]*files.FileReq, 0)
	form, err := ctx.MultipartForm()

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(uploadFilesErr),
			"invalid file",
		).Resp()
	}

	filesReq := form.File["files"]

	// file validte extension
	extMap := map[string]string{
		"png":  "png",
		"jpg":  "jpg",
		"jpeg": "jpeg",
	}

	for _, file := range filesReq {
		ext := strings.TrimPrefix(filepath.Ext(file.Filename), ".")

		if extMap[ext] != ext || extMap[ext] == "" {
			return entities.NewResponse(ctx).Error(
				fiber.ErrBadRequest.Code,
				string(uploadFilesErr),
				"invalid extension file",
			).Resp()
		}

		if file.Size > int64(h.conf.App().FileLimit()) {
			return entities.NewResponse(ctx).Error(
				fiber.ErrBadRequest.Code,
				string(uploadFilesErr),
				fmt.Sprintf("file size must less than %d MiB", int(math.Ceil(float64(h.conf.App().FileLimit())/math.Pow(1024, 2)))),
			).Resp()
		}

		req = append(req, &files.FileReq{
			File:      file,
			Extension: ext,
			FileName:  utilities.RandFileName(ext),
		})
	}

	results, err := h.filesUsecase.UploadToGCP(req)

	if err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(uploadFilesErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, results).Resp()
}

func (h *filesHandler) DeleteFiles(ctx *fiber.Ctx) error {
	req := make([]*files.DeleteFileReq, 0)

	if err := ctx.BodyParser(&req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrBadRequest.Code,
			string(deleteFilesErr),
			err.Error(),
		).Resp()
	}

	if err := h.filesUsecase.DeleteFilesOnGCP(req); err != nil {
		return entities.NewResponse(ctx).Error(
			fiber.ErrInternalServerError.Code,
			string(deleteFilesErr),
			err.Error(),
		).Resp()
	}

	return entities.NewResponse(ctx).Success(fiber.StatusOK, nil).Resp()
}
