package entities

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/taekub314/taekub-shop/pkg/logger"
)

type IResponse interface {
	Success(code int, data any) IResponse
	Error(code int, traceId string, msg string) IResponse
	Resp() error
}

type Response struct {
	StatusCode int
	Data       any
	ErrorRes   *ErrorResponse
	Context    *fiber.Ctx
	IsError    bool
}

type PaginationRes struct {
	Data      any `json:"data"`
	Page      int `json:"page"`
	PageSize  int `json:"page_size"`
	TotalPage int `json:"total_page"`
	TotalItem int `json:"total_item"`
}

type ErrorResponse struct {
	TraceId string `json:"trace_id"`
	Msg     string `json:"message"`
}

func NewResponse(ctx *fiber.Ctx) IResponse {
	return &Response{
		Context: ctx,
	}
}

func (res *Response) Success(code int, data any) IResponse {
	res.StatusCode = code
	res.Data = data

	logger.InitLogger(res.Context, &res.Data).Print()
	return res
}

func (res *Response) Error(code int, traceId string, msg string) IResponse {
	res.IsError = true
	res.StatusCode = code
	res.ErrorRes = &ErrorResponse{
		TraceId: traceId,
		Msg:     msg,
	}

	logger.InitLogger(res.Context, &res.ErrorRes).Print()
	return res
}

func (res *Response) Resp() error {
	return res.Context.Status(res.StatusCode).JSON(func() any {
		if res.IsError {
			return res.ErrorRes
		}

		return &res.Data
	}())
}
